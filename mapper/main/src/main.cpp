#include "StreetsDatabaseAPI.h"
#include "m1.h"
#include "m2.h"
#include "m3.h"
#include "m4.h"
#include <iostream>
#include <unordered_map>
//#include "path_varify.cpp"


using namespace std;

int main() {

 //draw_map("/cad2/ece297s/public/maps/newyork.bin");
   load_map("/cad2/ece297s/public/maps/toronto.bin");
    std::vector<unsigned> delivery_ids ={334, 4904, 6868, 9693, 11584, 16073, 19450, 22197, 23666, 24352, 24756, 24837, 26373, 28349, 28760, 29864, 33378, 44271, 45997, 53528, 54101, 59155, 59257, 59823, 63019, 67387, 68370, 69112, 71050, 71395, 75438, 85427, 86482, 97085, 103839, 105650, 109689, 119610, 122014, 129738, 137740, 140247, 141616, 143665, 143872, 144521, 152618, 157705, 159225, 163352};
    //{2329, 12330, 18825, 19724, 21752, 21962, 22528, 24598, 31686, 33236, 39338, 42709, 43735, 47109, 47814, 56874, 78752, 86098, 100663, 112486, 112539, 118951, 122254, 126194, 126539, 127242, 127663, 130487, 139127, 139584, 147591, 148112, 149819, 152685, 156801, 158718, 160019, 161839, 163431, 171776};
    std::vector<unsigned> depot_ids = {6604, 15321, 30063, 57131, 77774, 97918, 102219, 103141, 160373, 160522};
    vector<unsigned> tour = traveling_salesman(delivery_ids, depot_ids);
    double tour_cost = compute_path_travel_time(tour);
    std::cout << "Test Cost: " << tour_cost << std::endl;
//   find_path_between_intersections(160763, 31504);
//   find_path_between_intersections(14872, 93072);
//   find_path_between_intersections(96699, 134457);
//   find_path_between_intersections(160763, 31504);
//   find_path_between_intersections(145596, 165268);
//    find_path_between_intersections(120613, 160539);
//   find_path_between_intersections(49344, 70760);
//    find_path_between_intersections(149393, 108453);
//    find_path_between_intersections(149731, 159696);
//    find_path_between_intersections(27356, 142499);
//    find_path_between_intersections(150613, 48833);
  // find_street_street_segments("Blue Anchor Trail")
    
    close_map();
    
    return 0;
}
 
