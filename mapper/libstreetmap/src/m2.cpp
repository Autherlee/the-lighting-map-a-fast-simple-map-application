#include "m2.h"
#include "m3.h"
#include "m4.h"
#include <math.h>
#include <sstream>
#include <locale> 
using namespace std;

double ratio=0;
double start_lat=0;
double start_lon=0;
double aver_lag=0;
double Ave_lat=0;
double end_lat=0;
double end_lon=0;
unsigned long long IDfrom=0;
unsigned long long IDto=0;
string POI;
string findinter;
string findpath;
string findpathPOI;
string realPOIname;
string choosemap;
bool flag=false;
bool flagkey=false;
bool flagchange=false;
bool flaginter=false;
bool flagfindpath=false;
bool flagfindpathPOI=false;
bool checkfirst=false;
vector<unsigned> path;

void draw_map(std::string map_path){
    load_map(map_path);
    
    find_coordinate();
    t_bound_box initial_coords = t_bound_box(start_lon,start_lat,end_lon,end_lat);
    init_graphics("Map", BACKGROUND);
    set_visible_world(initial_coords);
    create_button ("Window", "PointofInterest", act_on_new_button_func);
    create_button ("PointofInterest", "SearchPOI", act_on_new_button_func_key);
    create_button ("SearchPOI", "ChooseMap", act_on_new_button_func_change);
    create_button ("ChooseMap", "FindIntersaction", act_on_new_button_func_intersection);
    create_button ("FindIntersaction", "FindPath", act_on_new_button_func_path);
    create_button ("FindPath", "FindPOIPath", act_on_new_button_func_poi);
    set_mouse_move_input (true);
    set_keypress_input (true);
    event_loop(act_on_button_press,NULL, act_on_key_press, drawscreen);
}

/*Draw the map screen*/
void drawscreen(void){
    set_draw_mode (DRAW_NORMAL);
    clearscreen();
    setlinewidth (1);
    setcolor (WHITE);
    {
        int FeaturePointCount =0;
        for(auto iter_features= Features.begin();iter_features!= Features.end();iter_features++){
          
            
            for(auto iter_id = iter_features->second.begin();iter_id != iter_features->second.end();iter_id++ ){
               FeaturePointCount = getFeaturePointCount(*iter_id);
               t_point Feature_Location[FeaturePointCount];
               for(int i=0 ; i<FeaturePointCount;i++){
                  t_point location = location_calculator(getFeaturePoint(*iter_id, i));
                  Feature_Location[i] = location ;
                } 
                 draw_feature(Feature_Location,FeaturePointCount,iter_features->first);
             
            }
           
           
        }
        /*
         make 4 different level, each contain different amount of streets
         */
         t_bound_box current_visable = get_visible_world();
         float area_screen = current_visable.area();
         int draw_street_flag = 0;
         if(area_screen > 0.03){
             draw_street_flag = 0;
         }
         if(area_screen < 0.03 && area_screen >0.01){
             draw_street_flag = 1;
         }
         if(area_screen <0.01 && area_screen>0.002){
             draw_street_flag = 2;
         }
         if(area_screen <0.002){
             draw_street_flag = 3;
         }
         
        draw_street(draw_street_flag);
        draw_highway();
        draw_street_name1();
        draw_street_name2();
        draw_street_name3();
        draw_street_name4();
        if(flag)
            draw_point_of_interest();
        
         if(flagkey){
      
                draw_search_POI();
        }
         else{
            POI.clear();
        }
        
        
        if(flagchange){
            draw_choose_map();
        }
        else{
            choosemap.clear();
        }
        
        if(flaginter){
            draw_intersection_map();
        }
        else{
             findinter.clear();
        }
        if(flagfindpath)
            draw_path_map(path);
        else{
            checkfirst=false;
            IDfrom=0;
            IDto=0;
            path.clear();
            findpath.clear();
        }
        if(flagfindpathPOI)
            draw_path_map(path);
        else{
            checkfirst=false;
            IDfrom=0;
             path.clear();
             realPOIname.clear();
            findpathPOI.clear();
        }
    }
}


void act_on_button_press (float x, float y, t_event_buttonPressed event) {
        std::cout << "User clicked a mouse button at coordinates (" 
             << x << "," << y << ")";
    if (event.shift_pressed || event.ctrl_pressed) {
            std::cout << " with ";
            if (event.shift_pressed) {
                    std::cout << "shift ";
                    if (event.ctrl_pressed)
                            std::cout << "and ";
            }
            if (event.ctrl_pressed) 
                    std::cout << "control ";
            std::cout << "pressed.";
    }
    std::cout << std::endl;
      if(flag||flagkey){
    LatLon coordinates;
    coordinates.lat=y;
    coordinates.lon=x/(cos(Ave_lat*DEG_TO_RAD));
    string name=find_closest_point_of_interest(coordinates);
    update_message(name);
     }
    if(flagfindpath){
    LatLon coordinates;
    coordinates.lat=y;
    coordinates.lon=x/(cos(Ave_lat*DEG_TO_RAD));
    
    if(IDfrom==0){
     drawscreen();
    IDfrom=find_closest_intersection(coordinates);
    }
    else if(IDfrom!=0){
        IDto=find_closest_intersection(coordinates);
        path=find_path_between_intersections(IDfrom,IDto);
        for(auto iter_path = path.begin(); iter_path != path.end();iter_path++){
        draw_PATH_FUN(*iter_path);
    }
        draw_instruction(path);
        double time=compute_path_travel_time(path);
        string T= to_string(time);
        string s="It will cost ";
        string min=" minutes to reach destination";
        s+=T;
        s+=min;
        update_message(s);
        IDfrom=0;
        IDto=0;
        path.clear();
    }
    
    }
}

void act_on_key_press (char c) {
    if(flagkey){
        if(c=='\r'){
            
               draw_search_POI();  
        }
        else if(c==8&&(!POI.empty())){
        POI.pop_back();
        update_message(POI);
        }
        else if(c!=0&&c!=8){
            POI+=c;
            update_message(POI);
        }  
    }
    
    if(flagchange){
        if(c=='\r'){
            if(choosemap=="1"){
                choosemap.clear();
                update_message(" ");
                initial();
                close_graphics();
                close_map();
                draw_map("/cad2/ece297s/public/maps/cairo_egypt.bin");
            }
            else if(choosemap=="2"){
                choosemap.clear();
                update_message(" ");
                initial();
                close_graphics();
                close_map();
                draw_map("/cad2/ece297s/public/maps/hamilton_canada.bin");
            }
            else if(choosemap=="3"){
                choosemap.clear();
                update_message(" ");
                initial();
                close_graphics();
                close_map();
                draw_map("/cad2/ece297s/public/maps/moscow.bin");
            }
            else if(choosemap=="4"){
                choosemap.clear();
                update_message(" ");
                initial();
                close_graphics();
                close_map();
                draw_map("/cad2/ece297s/public/maps/newyork.bin");
            }
            else if(choosemap=="5"){
                choosemap.clear();
                update_message(" ");
                initial();
                close_graphics();
                close_map();
                draw_map("/cad2/ece297s/public/maps/saint_helena.bin");
            }
            else if(choosemap=="6"){
                choosemap.clear();
                update_message(" ");
                initial();
                close_graphics();
                close_map();
                draw_map("/cad2/ece297s/public/maps/toronto.bin");
            }
            else if(choosemap=="7"){
                choosemap.clear();
                update_message(" ");
                initial();
                close_graphics();
                close_map();
                draw_map("/cad2/ece297s/public/maps/toronto_canada.bin");
            }
            else{
                choosemap.clear();
                update_message("Error: Please enter the correct map index");
            }    
        }
        else if(c==8&&(!choosemap.empty())){
        choosemap.pop_back();
        update_message(choosemap);
        }
        else if(c!=0&&c!=8){
            choosemap+=c;
            update_message(choosemap);
        }
    }
    
    if(flaginter){
        if(c=='\r'){
               draw_intersection_map();  
               drawscreen();
        }
        else if(c==8&&(!findinter.empty())){
        findinter.pop_back();
        update_message(findinter);
        }
        else if(c!=0&&c!=8){
            findinter+=c;
            update_message(findinter);
        }  
    }
    
    if(flagfindpath){
        if(c=='\r'){
            if(checkfirst==false){
                if(check(findpath))
                update_message("Please enter next intersection"); 
            }
            else if(checkfirst==true){
                 if(check(findpath)){
                      path=find_path_between_intersections(IDfrom,IDto);
                      for(auto iter_path = path.begin(); iter_path != path.end();iter_path++){
                         draw_PATH_FUN(*iter_path);
                        }
                      draw_instruction(path);
                     double time=compute_path_travel_time(path); 
                    string T= to_string(time);
                    string s="It will cost ";
                    string min=" minutes to reach destination";
                    s+=T;
                    s+=min;
                    update_message(s);
                }
            }
        }
                
        else if(c==8&&(!findpath.empty())){
        findpath.pop_back();
        update_message(findpath);
        }
        else if(c!=0&&c!=8){
            findpath+=c;
            update_message(findpath);
        }   
    }
    if(flagfindpathPOI){
        if(c=='\r'){
            if(checkfirst==false){
                if(check(findpathPOI))
                update_message("Please enter the point of interest");
                
            }
            else if(checkfirst==true){
                 if(checkPOI(findpathPOI)){
                path=find_path_to_point_of_interest(IDfrom,realPOIname);
                for(auto iter_path = path.begin(); iter_path != path.end();iter_path++){
                    draw_PATH_FUN(*iter_path);
                    }
                    draw_instruction(path);
                    double time=compute_path_travel_time(path);
                    string T= to_string(time);
                    string s="It will cost ";
                    string min=" minutes to reach destination";
                    s+=T;
                    s+=min;
                    update_message(s);
                 }
            }
        }
                
        else if(c==8&&(!findpathPOI.empty())){
        findpathPOI.pop_back();
        update_message(findpathPOI);
        }
        else if(c!=0&&c!=8){
            findpathPOI+=c;
            update_message(findpathPOI);
        }   
        }
  
}
void act_on_new_button_func (void (*drawscreen_ptr) (void)) {
    flag=!flag;  
    drawscreen_ptr ();
     
} 

void act_on_new_button_func_key (void (*drawscreen_ptr) (void)) {
    flagkey=!flagkey;
    if(flagkey)
        update_message("Please enter the name of the POI");
    else
        update_message(" ");
    drawscreen_ptr ();
     
}  

void act_on_new_button_func_change (void (*drawscreen_ptr) (void)) {
    flagchange=!flagchange;
    if(flagchange)
        update_message("Please enter the index of the map");
    else
        update_message(" ");
    drawscreen_ptr ();
     
}  

void act_on_new_button_func_intersection(void (*drawscreen_ptr) (void)){
    flaginter=!flaginter;
    if(flaginter)
        update_message("Please enter the name of the intersection");
    else{
        update_message(" ");
        set_visible_world(start_lon,start_lat,end_lon,end_lat);
    }
    drawscreen_ptr ();
}

void act_on_new_button_func_path(void (*drawscreen_ptr) (void)){
    flagfindpath=!flagfindpath;
    if(flagfindpath)
        update_message("Please enter the name of the first intersection");
    else{
        update_message(" ");
        findpath.clear();
        path.clear();
    }
    drawscreen_ptr ();
}

void act_on_new_button_func_poi(void (*drawscreen_ptr) (void)){
    flagfindpathPOI=!flagfindpathPOI;
    if(flagfindpathPOI)
        update_message("Please enter the name of the intersection");
    else{
        update_message(" ");
        findpathPOI.clear();
        path.clear();
    }
    drawscreen_ptr ();
}
/*convert longitude and latitude into x,y coordinate */
t_point location_calculator(LatLon coordinates){
    t_point coord;
    coord.y = (coordinates.lat);
    coord.x = (coordinates.lon*cos(Ave_lat*DEG_TO_RAD));
    
    return coord;
 }
/*draw different feature with different color on map*/
void draw_feature(t_point* points, int npoints, string name) {
    bool isClose = false;
    t_point Feature_Location[npoints+1];
    if ((points[0].x == points[npoints-1].x)&&(points[0].y == points[npoints-1].y)) {
        isClose = true;
    }
    /*use blue to represent attribute of water*/
    if ((name == "water")||(name == "river")||(name == "rapids")) {
        setcolor(LAKE);
    }
    else if((name == "lake")){
        setcolor(LAKE);
        if(!isClose){
           
            for(int i =0;i<npoints;i++){
                Feature_Location[i] = points[i];
            }
            
            Feature_Location[npoints] = points[0];   
        }
    }
    else if (name == "wood") {
        setcolor(GRASSLAND); /*use green to represent attribute of wood*/
    }
    else if ((name == "grassland")||(name == "park")||(name == "national_park")) {
        setcolor(153,255 ,153);
    }
    else if ((name == "tree_row")||(name == "tree")||(name == "scrub")) {
        setcolor(DARKGREEN);/*use green to represent attribute of trees*/
    }
    else if ((name == "sand")||(name == "beach")||(name == "ditch")) {
        setcolor(LIGHTGREY);/*use grey to represent attribute of beach*/
    }
    else if ((name == "pond")||(name == "lagoon")||(name == "stream")) {
        setcolor(LAKE);
    }
    else if ((name == "boatyard")||(name == "wetland")) {
        setcolor(MEDIUMPURPLE);
    }
    else if ((name == "island")||(name == "land")||(name == "heath")) {
        setcolor(BACKGROUND);
    }
    else if ((name == "drain")||(name == "weir")||(name == "breakwater")||(name == "cliff")||(name == "dam")||(name == "riverbank")) {
        setcolor(DARKGREY);
    }
    else if (name == "peak") {
        setcolor(SADDLEBROWN);
    }
   
    else {
        setcolor(BACKGROUND);
    }
    
    if (isClose)
    {
        fillpoly(points,npoints);
    }   
    
    else {
        if(name !="lake"){
        for (int i = 0;i < npoints-1;i++) {
            drawline(points[i].x,points[i].y,points[i+1].x,points[i+1].y);
            }
        }
        else{
           fillpoly(Feature_Location,npoints+1);
        }
    }
}
/*draw points of interest on map */
void draw_point_of_interest(){
    t_bound_box curr_visi = get_visible_world();
    double dx=curr_visi.get_width();
    unsigned number_of_point_of_interest= getNumberOfPointsOfInterest();
 for(unsigned i=0;i<number_of_point_of_interest;i++){
     LatLon location=getPointOfInterestPosition(i);
     t_point coordinate=location_calculator(location);
     setcolor(255,153,153);
     fillarc (coordinate.x,coordinate.y,dx/200,0,360);
     setcolor(WHITE);
     settextattrs(6,0); 
     drawtext(coordinate.x, coordinate.y,  "P",0.0002,0.0002);
}
}

void draw_search_POI(){
    if(POI.size()!=0){
    t_bound_box curr_visi = get_visible_world();
    double dx=curr_visi.get_width();
    unsigned number_of_point_of_interest= getNumberOfPointsOfInterest();
    string name;
    bool check=false;
   for(unsigned i=0;i<number_of_point_of_interest;i++){
     name=getPointOfInterestName(i);
     name=convert_to_low(name);
     POI=convert_to_low(POI);
     
     if(std::string::npos!=name.find(POI)){
     check=true;
     LatLon location=getPointOfInterestPosition(i);
     t_point coordinate=location_calculator(location);
     setcolor(255,153,153);
     fillarc (coordinate.x,coordinate.y,dx/100,0,360);
     setcolor(WHITE);
     settextattrs(10,0); 
     drawtext(coordinate.x, coordinate.y,  "P",0.0002,0.0002);
     }
}
    update_message(POI);
    if(check==false){
        update_message("Error:Wrong name please enter again");
        POI.clear();
    }
}
}

void draw_choose_map(){
    double start_lon1=-57.6848;
    double start_lat1=43.4801;
    double end_lon1=-57.1144;
    double end_lat1=43.9191;
    clearscreen();
    t_bound_box a = t_bound_box(start_lon1,start_lat1,end_lon1,end_lat1);
    set_visible_world(start_lon1,start_lat1,end_lon1,end_lat1);
    setcolor(CYAN);
    fillrect(a);
    setcolor(WHITE);
     settextattrs(30,0); 
     drawtext((start_lon1+(end_lon1-start_lon1)/2),end_lat1*0.9990778 ,  "Choose Map",1,1);
     setcolor(LIGHTPINK);
     t_bound_box one = t_bound_box(start_lon1*0.9964393,start_lat1*1.0079208,end_lon1*1.0035559,end_lat1*0.99845167);
     fillrect(one);
     setcolor(BLACK);
     settextattrs(13,0);
     drawtext_in(one,"1.Cairo Egypt");
     setcolor(LIGHTPINK);
     t_bound_box two = t_bound_box(start_lon1*0.9964393,start_lat1*1.0079208*0.999044509,end_lon1*1.0035559,end_lat1*0.99845167*0.999044509);
     fillrect(two);
     setcolor(BLACK);
     drawtext_in(two,"2.Hamilton Canada");
     setcolor(LIGHTPINK);
     t_bound_box three = t_bound_box(start_lon1*0.9964393,start_lat1*1.0079208*(pow (0.999044509, 2)),end_lon1*1.0035559,end_lat1*0.99845167*(pow (0.999044509, 2)));
     fillrect(three);
     setcolor(BLACK);
     drawtext_in(three,"3.Moscow");
     setcolor(LIGHTPINK);
     t_bound_box four = t_bound_box(start_lon1*0.9964393,start_lat1*1.0079208*(pow (0.999044509, 3)),end_lon1*1.0035559,end_lat1*0.99845167*(pow (0.999044509, 3)));
     fillrect(four);
     setcolor(BLACK);
     drawtext_in(four,"4.New York");
     setcolor(LIGHTPINK);
     t_bound_box five = t_bound_box(start_lon1*0.9964393,start_lat1*1.0079208*(pow (0.999044509, 4)),end_lon1*1.0035559,end_lat1*0.99845167*(pow (0.999044509, 4)));
     fillrect(five);
     setcolor(BLACK);
     drawtext_in(five,"5.Saint Helena");
     setcolor(LIGHTPINK);
     t_bound_box six = t_bound_box(start_lon1*0.9964393,start_lat1*1.0079208*(pow (0.999044509, 5)),end_lon1*1.0035559,end_lat1*0.99845167*(pow (0.999044509, 5)));
     fillrect(six);
     setcolor(BLACK);
     drawtext_in(six,"6.Toronto");
     setcolor(LIGHTPINK);
     t_bound_box seven = t_bound_box(start_lon1*0.9964393,start_lat1*1.0079208*(pow (0.999044509, 6)),end_lon1*1.0035559,end_lat1*0.99845167*(pow (0.999044509, 6)));
     fillrect(seven);
     setcolor(BLACK);
     drawtext_in(seven,"7.Toronto Canada");
}

void draw_intersection_map(){
    if(findinter.size()!=0){
    unsigned number_of_intersection= getNumberOfIntersections();
    string name;
    int max=-1;
    bool check=false;
    int tempcount=0;
    int validcount=0;
   for(unsigned i=0;i<number_of_intersection;i++){
     name=getIntersectionName(i);
     name=convert_to_low(name);
     findinter=convert_to_low(findinter);
      stringstream line(findinter);
     while(!line.eof()){
         string output;
         line>>output;
         if(std::string::npos!=name.find(output))
             validcount++;
     }
     if(validcount>tempcount){
         max=i;
         tempcount=validcount;
         validcount=0;
     }
     else
         validcount=0;
}        
     if(max!=-1){
     check=true;
     LatLon location=getIntersectionPosition(max);
      t_point coordinate=location_calculator(location);
     setcolor(BLUE);
     fillarc (coordinate.x,coordinate.y,0.0002,0,360);
     setcolor(WHITE);
     settextattrs(10,0); 
     drawtext(coordinate.x, coordinate.y,  "I",0.0002,0.0002);
     set_visible_world(coordinate.x-0.0082,coordinate.y-0.0079,coordinate.x+0.0077,coordinate.y+0.0009);
     
     }      

    update_message(findinter);
    if(check==false){
        update_message("Error:Wrong name please enter again");
        findinter.clear();
    }
    } 
}

void draw_path_map(vector<unsigned> path){
    if(path.size()!=0){
      for(auto iter_path = path.begin(); iter_path != path.end();iter_path++){
        draw_PATH_FUN(*iter_path);
                    double time=compute_path_travel_time(path);
                    string T= to_string(time);
                    string s="It will cost ";
                    string min=" min to reach destination";
                    s+=T;
                    s+=min;
                    update_message(s);
    }
    }
    
}

bool check(string namepath){
    if(namepath.size()!=0){
        unsigned number_of_intersection= getNumberOfIntersections();
        string name;
        int max=-1;
        int tempcount=0;
        int validcount=0;
        for(unsigned i=0;i<number_of_intersection;i++){
            name=getIntersectionName(i);
            name=convert_to_low(name);
            findpath=convert_to_low(namepath);
            stringstream line(findpath);
            while(!line.eof()){
                string output;
                line>>output;
                if(std::string::npos!=name.find(output))
                    validcount++;
            }
            if(validcount>tempcount){
                max=i;
                tempcount=validcount;
                validcount=0;
            }
            else
                validcount=0;
        }
        if(max!=-1&&checkfirst==false){
            IDfrom=max;
            checkfirst=true;
            if(flagfindpath)
                findpath.clear();
            else if(flagfindpathPOI)
                findpathPOI.clear();
            return true;
        }
        else if(max!=-1&&checkfirst==true){
            IDto=max;
            if(flagfindpath)
                findpath.clear();
            else if(flagfindpathPOI)
                findpathPOI.clear();
            cout<<IDfrom<<endl;
            cout<<IDto<<endl;
            return true;
        }
   
        else {
            update_message("Error:Wrong name please enter again");
            if(flagfindpath)
                findpath.clear();
            else if(flagfindpathPOI)
                findpathPOI.clear();
            checkfirst=false;
            return false;
        }
    }
    return false;
}
bool checkPOI(string namePOI){
    if(namePOI.size()!=0){
        unsigned number_of_point_of_interest= getNumberOfPointsOfInterest();
        string name;
        for(unsigned i=0;i<number_of_point_of_interest;i++){
            realPOIname=getPointOfInterestName(i);
            name=convert_to_low(realPOIname);
            namePOI=convert_to_low(namePOI);
            if((std::string::npos!=name.find(namePOI))&&checkfirst==true){
                return true;
            }
        }
        update_message("Error:Wrong name please enter again");
        findpathPOI.clear();
        checkfirst=false;
        return false;
    }
    return false;
}
/*draw different level of streets with different color and draw different level of detail with different 
 zoom in ratio
*/
void draw_street(int draw_street_flag){
    setlinestyle (SOLID);
        for(auto iter = Streetsegment_map.begin();iter!=Streetsegment_map.end();iter++ ){
            for (auto it = (iter->second).begin();it != (iter->second).end();it++){
                double Limit = getStreetSegmentSpeedLimit(*it);
                if (Limit < 81) {
                    StreetSegmentEnds from_to =  getStreetSegmentEnds(*it);
                    LatLon pos = getIntersectionPosition(from_to.from);
                    LatLon pos_1 = getIntersectionPosition(from_to.to);
                    
                    int count = getStreetSegmentCurvePointCount(*it);
                    t_point coord_inter = location_calculator(pos);
                    t_point coord_inter_1 = location_calculator(pos_1);
                    if (count == 0) { /*
                                       draw streets that do not has curve point
                                       */
                        if(draw_street_flag ==0){
                            auto street_name = Street_map_level1.find(iter->first);
                            if(street_name != Street_map_level1.end()){
                                setlinewidth (5);
                                setcolor (191,191,178);
                                drawline(coord_inter.x,coord_inter.y,coord_inter_1.x,coord_inter_1.y);
                                setlinewidth (3);
                                setcolor (WHITE);
                                drawline(coord_inter.x,coord_inter.y,coord_inter_1.x,coord_inter_1.y);
                            }    
                        }
                        if(draw_street_flag == 1){
                            auto street_name = Street_map_level1.find(iter->first);
                            if(street_name != Street_map_level1.end()){
                                setlinewidth (6);
                                setcolor (191,191,178);
                                drawline(coord_inter.x,coord_inter.y,coord_inter_1.x,coord_inter_1.y);
                                setlinewidth (4);
                                setcolor (WHITE);
                                drawline(coord_inter.x,coord_inter.y,coord_inter_1.x,coord_inter_1.y);
                            }
                            auto street_name_1 = Street_map_level2.find(iter->first);
                            if(street_name_1 != Street_map_level2.end()){
                                setlinewidth (3);
                                setcolor (LIGHTGREY);
                                drawline(coord_inter.x,coord_inter.y,coord_inter_1.x,coord_inter_1.y);
                            }
                        }
                        if(draw_street_flag ==2){
                            auto street_name = Street_map_level1.find(iter->first);
                            if(street_name != Street_map_level1.end()){
                                setlinewidth (7);
                                setcolor (191,191,178);
                                drawline(coord_inter.x,coord_inter.y,coord_inter_1.x,coord_inter_1.y);
                                setlinewidth (5);
                                setcolor (WHITE);
                                drawline(coord_inter.x,coord_inter.y,coord_inter_1.x,coord_inter_1.y);
                            }
                            auto street_name_1 = Street_map_level2.find(iter->first);
                            if(street_name_1 != Street_map_level2.end()){
                                setlinewidth (3);
                                setcolor (LIGHTGREY);
                                drawline(coord_inter.x,coord_inter.y,coord_inter_1.x,coord_inter_1.y);
                            }
                            auto street_name_2 = Street_map_level3.find(iter->first);
                            if(street_name_2 != Street_map_level3.end()){
                                setlinewidth (3);
                                setcolor (LIGHTGREY);
                                drawline(coord_inter.x,coord_inter.y,coord_inter_1.x,coord_inter_1.y);
                            }
                        }
                        if(draw_street_flag ==3){
                            auto street_name = Street_map_level1.find(iter->first);
                            if(street_name != Street_map_level1.end()){
                                setlinewidth (9);
                                setcolor (191,191,178);
                                drawline(coord_inter.x,coord_inter.y,coord_inter_1.x,coord_inter_1.y);
                                setlinewidth (7);
                                setcolor (WHITE);
                                drawline(coord_inter.x,coord_inter.y,coord_inter_1.x,coord_inter_1.y);
                            }
                            auto street_name_1 = Street_map_level2.find(iter->first);
                            if(street_name_1 != Street_map_level2.end()){
                                setlinewidth (4);
                                setcolor (WHITE);
                                drawline(coord_inter.x,coord_inter.y,coord_inter_1.x,coord_inter_1.y);
                            }
                            auto street_name_2 = Street_map_level3.find(iter->first);
                            if(street_name_2 != Street_map_level3.end()){
                                setlinewidth (4);
                                setcolor (WHITE);
                                drawline(coord_inter.x,coord_inter.y,coord_inter_1.x,coord_inter_1.y);
                            }
                            auto street_name_3 = Street_map_level4.find(iter->first);
                            if(street_name_3 != Street_map_level4.end()){
                                setlinewidth (4);
                                setcolor (WHITE);
                                drawline(coord_inter.x,coord_inter.y,coord_inter_1.x,coord_inter_1.y);
                            }
                        }
                       
                    }
                    else {
                        /*draw streets that have curve points*/
                        if(draw_street_flag == 0){
                        auto street_name = Street_map_level1.find(iter->first);
                        if(street_name != Street_map_level1.end()) {
                        t_point curve_tpoint = location_calculator(getStreetSegmentCurvePoint(*it,0));
                        setlinewidth (5);
                        setcolor (191,191,178);
                        drawline(coord_inter.x,coord_inter.y,curve_tpoint.x,curve_tpoint.y);
                        setlinewidth (3);
                        setcolor (WHITE);
                        drawline(coord_inter.x,coord_inter.y,curve_tpoint.x,curve_tpoint.y);
                        for (int i = 0; i < count-1; i++) {
                            LatLon curve = getStreetSegmentCurvePoint(*it,i);
                            t_point temp = location_calculator(curve);
                            LatLon curve_1 = getStreetSegmentCurvePoint(*it,i+1);
                            t_point temp_1 = location_calculator(curve_1);
                            setlinewidth (5);
                            setcolor (191,191,178);
                            drawline(temp.x,temp.y,temp_1.x,temp_1.y);
                            setlinewidth (3);
                            setcolor (WHITE);
                            drawline(temp.x,temp.y,temp_1.x,temp_1.y);
                        }
                        curve_tpoint = location_calculator(getStreetSegmentCurvePoint(*it,count-1));
                        auto street_name_1 = Street_map_level1.find(iter->first);
                        if(street_name_1 != Street_map_level1.end())
                        setlinewidth (5);
                        setcolor (191,191,178);
                        drawline(curve_tpoint.x,curve_tpoint.y,coord_inter_1.x,coord_inter_1.y);
                        setlinewidth (3);
                        setcolor (WHITE);
                        drawline(curve_tpoint.x,curve_tpoint.y,coord_inter_1.x,coord_inter_1.y);
                        }
                        }
                        
                        if(draw_street_flag == 1){
                        auto street_name = Street_map_level1.find(iter->first);
                        if(street_name != Street_map_level1.end()) {
                        t_point curve_tpoint = location_calculator(getStreetSegmentCurvePoint(*it,0));
                        setlinewidth (6);
                        setcolor (191,191,178);
                        drawline(coord_inter.x,coord_inter.y,curve_tpoint.x,curve_tpoint.y);
                        setlinewidth (4);
                        setcolor (WHITE);
                        drawline(coord_inter.x,coord_inter.y,curve_tpoint.x,curve_tpoint.y);
                        for (int i = 0; i < count-1; i++) {
                            LatLon curve = getStreetSegmentCurvePoint(*it,i);
                            t_point temp = location_calculator(curve);
                            LatLon curve_1 = getStreetSegmentCurvePoint(*it,i+1);
                            t_point temp_1 = location_calculator(curve_1);
                            setlinewidth (6);
                            setcolor (191,191,178);
                            drawline(temp.x,temp.y,temp_1.x,temp_1.y);
                            setlinewidth (4);
                            setcolor (WHITE);
                            drawline(temp.x,temp.y,temp_1.x,temp_1.y);
                        }
                        curve_tpoint = location_calculator(getStreetSegmentCurvePoint(*it,count-1));
                        auto street_name_1 = Street_map_level1.find(iter->first);
                        if(street_name_1 != Street_map_level1.end())
                        setlinewidth (6);
                        setcolor (191,191,178);
                        drawline(curve_tpoint.x,curve_tpoint.y,coord_inter_1.x,coord_inter_1.y);
                        setlinewidth (4);
                        setcolor (WHITE);
                        drawline(curve_tpoint.x,curve_tpoint.y,coord_inter_1.x,coord_inter_1.y);
                        }
                        
                        auto street_name_2 = Street_map_level2.find(iter->first);
                        if(street_name_2 != Street_map_level2.end()) {
                        t_point curve_tpoint = location_calculator(getStreetSegmentCurvePoint(*it,0));
                        setlinewidth (3);
                        setcolor (LIGHTGREY);
                        drawline(coord_inter.x,coord_inter.y,curve_tpoint.x,curve_tpoint.y);
                        for (int i = 0; i < count-1; i++) {
                            LatLon curve = getStreetSegmentCurvePoint(*it,i);
                            t_point temp = location_calculator(curve);
                            LatLon curve_1 = getStreetSegmentCurvePoint(*it,i+1);
                            t_point temp_1 = location_calculator(curve_1);
                            drawline(temp.x,temp.y,temp_1.x,temp_1.y);
                        }
                        curve_tpoint = location_calculator(getStreetSegmentCurvePoint(*it,count-1));
                        auto street_name_3 = Street_map_level2.find(iter->first);
                        if(street_name_3 != Street_map_level2.end())
                        drawline(curve_tpoint.x,curve_tpoint.y,coord_inter_1.x,coord_inter_1.y);
                        }
                        
                        
                        }
                        
                        
                        
                        if(draw_street_flag == 2){
                        auto street_name = Street_map_level1.find(iter->first);
                        if(street_name != Street_map_level1.end()) {
                        t_point curve_tpoint = location_calculator(getStreetSegmentCurvePoint(*it,0));
                        setlinewidth (7);
                        setcolor (191,191,178);
                        drawline(coord_inter.x,coord_inter.y,curve_tpoint.x,curve_tpoint.y);
                        setlinewidth (5);
                        setcolor (WHITE);
                        drawline(coord_inter.x,coord_inter.y,curve_tpoint.x,curve_tpoint.y);
                        for (int i = 0; i < count-1; i++) {
                            LatLon curve = getStreetSegmentCurvePoint(*it,i);
                            t_point temp = location_calculator(curve);
                            LatLon curve_1 = getStreetSegmentCurvePoint(*it,i+1);
                            t_point temp_1 = location_calculator(curve_1);
                            setlinewidth (7);
                            setcolor (191,191,178);
                            drawline(temp.x,temp.y,temp_1.x,temp_1.y);
                            setlinewidth (5);
                            setcolor (WHITE);
                            drawline(temp.x,temp.y,temp_1.x,temp_1.y);
                            
                        }
                        curve_tpoint = location_calculator(getStreetSegmentCurvePoint(*it,count-1));
                        auto street_name_1 = Street_map_level1.find(iter->first);
                        if(street_name_1 != Street_map_level1.end()){
                        setlinewidth (7);
                        setcolor (191,191,178);
                        drawline(curve_tpoint.x,curve_tpoint.y,coord_inter_1.x,coord_inter_1.y);
                        setlinewidth (5);
                        setcolor (WHITE);
                        drawline(curve_tpoint.x,curve_tpoint.y,coord_inter_1.x,coord_inter_1.y);
                        }
                        }
                        
                        auto street_name_2 = Street_map_level2.find(iter->first);
                        if(street_name_2 != Street_map_level2.end()) {
                        t_point curve_tpoint = location_calculator(getStreetSegmentCurvePoint(*it,0));
                        setlinewidth (3);
                        setcolor (LIGHTGREY);
                        drawline(coord_inter.x,coord_inter.y,curve_tpoint.x,curve_tpoint.y);
                        for (int i = 0; i < count-1; i++) {
                            LatLon curve = getStreetSegmentCurvePoint(*it,i);
                            t_point temp = location_calculator(curve);
                            LatLon curve_1 = getStreetSegmentCurvePoint(*it,i+1);
                            t_point temp_1 = location_calculator(curve_1);
                            drawline(temp.x,temp.y,temp_1.x,temp_1.y);
                        }
                        curve_tpoint = location_calculator(getStreetSegmentCurvePoint(*it,count-1));
                        auto street_name_3 = Street_map_level2.find(iter->first);
                        if(street_name_3 != Street_map_level2.end())
                        setlinewidth (3);
                        setcolor (LIGHTGREY);
                        drawline(curve_tpoint.x,curve_tpoint.y,coord_inter_1.x,coord_inter_1.y);
                        }
                        
                        auto street_name_4 = Street_map_level3.find(iter->first);
                        if(street_name_4 != Street_map_level3.end()) {
                        t_point curve_tpoint = location_calculator(getStreetSegmentCurvePoint(*it,0));
                        setlinewidth (3);
                        setcolor (LIGHTGREY);
                        drawline(coord_inter.x,coord_inter.y,curve_tpoint.x,curve_tpoint.y);
                        for (int i = 0; i < count-1; i++) {
                            LatLon curve = getStreetSegmentCurvePoint(*it,i);
                            t_point temp = location_calculator(curve);
                            LatLon curve_1 = getStreetSegmentCurvePoint(*it,i+1);
                            t_point temp_1 = location_calculator(curve_1);
                            drawline(temp.x,temp.y,temp_1.x,temp_1.y);
                        }
                        curve_tpoint = location_calculator(getStreetSegmentCurvePoint(*it,count-1));
                        auto street_name_1 = Street_map_level3.find(iter->first);
                        if(street_name_1 != Street_map_level3.end())
                        drawline(curve_tpoint.x,curve_tpoint.y,coord_inter_1.x,coord_inter_1.y);
                        }
                        
                        
                        }
                        
                        if(draw_street_flag == 3){
                         auto street_name = Street_map_level1.find(iter->first);
                        if(street_name != Street_map_level1.end()) {
                        t_point curve_tpoint = location_calculator(getStreetSegmentCurvePoint(*it,0));
                        setlinewidth (9);
                        setcolor (191,191,178);
                        drawline(coord_inter.x,coord_inter.y,curve_tpoint.x,curve_tpoint.y);
                        setlinewidth (7);
                        setcolor (WHITE);
                        drawline(coord_inter.x,coord_inter.y,curve_tpoint.x,curve_tpoint.y);
                        for (int i = 0; i < count-1; i++) {
                            LatLon curve = getStreetSegmentCurvePoint(*it,i);
                            t_point temp = location_calculator(curve);
                            LatLon curve_1 = getStreetSegmentCurvePoint(*it,i+1);
                            t_point temp_1 = location_calculator(curve_1);
                            setlinewidth (9);
                            setcolor (191,191,178);
                            drawline(temp.x,temp.y,temp_1.x,temp_1.y);
                            setlinewidth (7);
                            setcolor (WHITE);
                            drawline(temp.x,temp.y,temp_1.x,temp_1.y);
                        }
                        curve_tpoint = location_calculator(getStreetSegmentCurvePoint(*it,count-1));
                        auto street_name_1 = Street_map_level1.find(iter->first);
                        if(street_name_1 != Street_map_level1.end()){
                        setlinewidth (9);
                        setcolor (191,191,178);
                        drawline(curve_tpoint.x,curve_tpoint.y,coord_inter_1.x,coord_inter_1.y);    
                        setlinewidth (7);
                        setcolor (WHITE);
                        drawline(curve_tpoint.x,curve_tpoint.y,coord_inter_1.x,coord_inter_1.y);
                        }
                        }
                        
                        auto street_name_2 = Street_map_level2.find(iter->first);
                        if(street_name_2 != Street_map_level2.end()) {
                        t_point curve_tpoint = location_calculator(getStreetSegmentCurvePoint(*it,0));
                        setlinewidth (4);
                        setcolor (WHITE);
                        drawline(coord_inter.x,coord_inter.y,curve_tpoint.x,curve_tpoint.y);
                        for (int i = 0; i < count-1; i++) {
                            LatLon curve = getStreetSegmentCurvePoint(*it,i);
                            t_point temp = location_calculator(curve);
                            LatLon curve_1 = getStreetSegmentCurvePoint(*it,i+1);
                            t_point temp_1 = location_calculator(curve_1);
                            drawline(temp.x,temp.y,temp_1.x,temp_1.y);
                        }
                        curve_tpoint = location_calculator(getStreetSegmentCurvePoint(*it,count-1));
                        auto street_name_3 = Street_map_level2.find(iter->first);
                        if(street_name_3 != Street_map_level2.end())
                        setlinewidth (4);
                        setcolor (WHITE);
                        drawline(curve_tpoint.x,curve_tpoint.y,coord_inter_1.x,coord_inter_1.y);
                        }
                        
                        auto street_name_4 = Street_map_level3.find(iter->first);
                        if(street_name_4 != Street_map_level3.end()) {
                        t_point curve_tpoint = location_calculator(getStreetSegmentCurvePoint(*it,0));
                        setlinewidth (4);
                        setcolor (WHITE);
                        drawline(coord_inter.x,coord_inter.y,curve_tpoint.x,curve_tpoint.y);
                        for (int i = 0; i < count-1; i++) {
                            LatLon curve = getStreetSegmentCurvePoint(*it,i);
                            t_point temp = location_calculator(curve);
                            LatLon curve_1 = getStreetSegmentCurvePoint(*it,i+1);
                            t_point temp_1 = location_calculator(curve_1);
                            drawline(temp.x,temp.y,temp_1.x,temp_1.y);
                        }
                        curve_tpoint = location_calculator(getStreetSegmentCurvePoint(*it,count-1));
                        auto street_name_5 = Street_map_level3.find(iter->first);
                        if(street_name_5 != Street_map_level3.end())
                        drawline(curve_tpoint.x,curve_tpoint.y,coord_inter_1.x,coord_inter_1.y);
                        }
                        
                        auto street_name_6 = Street_map_level4.find(iter->first);
                        if(street_name_6 != Street_map_level4.end()) {
                        t_point curve_tpoint = location_calculator(getStreetSegmentCurvePoint(*it,0));
                        setlinewidth (4);
                        setcolor (WHITE);
                        drawline(coord_inter.x,coord_inter.y,curve_tpoint.x,curve_tpoint.y);
                        for (int i = 0; i < count-1; i++) {
                            LatLon curve = getStreetSegmentCurvePoint(*it,i);
                            t_point temp = location_calculator(curve);
                            LatLon curve_1 = getStreetSegmentCurvePoint(*it,i+1);
                            t_point temp_1 = location_calculator(curve_1);
                            drawline(temp.x,temp.y,temp_1.x,temp_1.y);
                        }
                        curve_tpoint = location_calculator(getStreetSegmentCurvePoint(*it,count-1));
                        auto street_name_7 = Street_map_level4.find(iter->first);
                        if(street_name_7 != Street_map_level4.end())
                        drawline(curve_tpoint.x,curve_tpoint.y,coord_inter_1.x,coord_inter_1.y);
                        }
                        }
                        
                    }
               
                }
            }
        }
    
   
   
    return ;  
}
/*classify highway with speed limit great than 81km/h and draw them with color orange */
void draw_highway(){
    setlinestyle (SOLID);
    for(auto iter = Streetsegment_map.begin();iter!=Streetsegment_map.end();iter++ ){
        for (auto it = (iter->second).begin();it != (iter->second).end();it++){
            double Limit = getStreetSegmentSpeedLimit(*it);
            if (Limit >= 81) {
                    
                StreetSegmentEnds from_to =  getStreetSegmentEnds(*it);
                LatLon pos = getIntersectionPosition(from_to.from);
                LatLon pos_1 = getIntersectionPosition(from_to.to);
                int count = getStreetSegmentCurvePointCount(*it);
                t_point coord_inter = location_calculator(pos);
                t_point coord_inter_1 = location_calculator(pos_1);
                setlinewidth (5);
                setcolor(BLACK);
                if (count == 0) {/*
                          draw highways that do not have curve point
                          */
                    drawline(coord_inter.x,coord_inter.y,coord_inter_1.x,coord_inter_1.y);
                }
            else {
                t_point curve_tpoint = location_calculator(getStreetSegmentCurvePoint(*it,0));
                drawline(coord_inter.x,coord_inter.y,curve_tpoint.x,curve_tpoint.y);
                for (int i = 0; i < count-1; i++) {
                    LatLon curve = getStreetSegmentCurvePoint(*it,i);
                    t_point temp = location_calculator(curve);
                    LatLon curve_1 = getStreetSegmentCurvePoint(*it,i+1);
                    t_point temp_1 = location_calculator(curve_1);
                    drawline(temp.x,temp.y,temp_1.x,temp_1.y);
                }
                curve_tpoint = location_calculator(getStreetSegmentCurvePoint(*it,count-1));
                drawline(curve_tpoint.x,curve_tpoint.y,coord_inter_1.x,coord_inter_1.y);
            }
            setlinewidth(3);
            setcolor(HIGHWAY);
            if (count == 0) {
                drawline(coord_inter.x,coord_inter.y,coord_inter_1.x,coord_inter_1.y);
                }
            else {/*
               draw highways that have curve point
               */
                t_point curve_tpoint = location_calculator(getStreetSegmentCurvePoint(*it,0));
                drawline(coord_inter.x,coord_inter.y,curve_tpoint.x,curve_tpoint.y);
                for (int i = 0; i < count-1; i++) {
                    LatLon curve = getStreetSegmentCurvePoint(*it,i);
                    t_point temp = location_calculator(curve);
                    LatLon curve_1 = getStreetSegmentCurvePoint(*it,i+1);
                    t_point temp_1 = location_calculator(curve_1);
                    drawline(temp.x,temp.y,temp_1.x,temp_1.y);
                    }
                curve_tpoint = location_calculator(getStreetSegmentCurvePoint(*it,count-1));
                drawline(curve_tpoint.x,curve_tpoint.y,coord_inter_1.x,coord_inter_1.y);
            }
        }
    }
    }
     find_path_between_intersections(2329,105070);
   // find_path_to_point_of_interest(96166, "KFC");
//     std::vector<unsigned> delivery_ids = {2329, 12330, 18825, 19724, 21752, 21962, 22528, 24598, 31686, 33236, 39338, 42709, 43735, 47109, 47814, 56874, 78752, 86098, 100663, 112486, 112539, 118951, 122254, 126194, 126539, 127242, 127663, 130487, 139127, 139584, 147591, 148112, 149819, 152685, 156801, 158718, 160019, 161839, 163431, 171776};
//    std::vector<unsigned> depot_ids = {5000, 14178, 19173, 43253, 69775, 74531, 105070, 118483, 121711, 130296};
//    vector<unsigned> tour = traveling_salesman(delivery_ids, depot_ids);
//    double tour_cost = compute_path_travel_time(tour);
//    std::cout << "Test_Easy_0 Cost: " << tour_cost << std::endl;
//    
    
}
/*draw street name on map, main street will draw first */
void draw_street_name1(){
    int segmentcount=0;
    for(auto iter = Streetsegment_map.begin();iter!=Streetsegment_map.end();iter++ ){
        for (auto it = (iter->second).begin();it != (iter->second).end();it++){
            StreetSegmentEnds from_to =  getStreetSegmentEnds(*it);
            LatLon pos = getIntersectionPosition(from_to.from);
            LatLon pos_1 = getIntersectionPosition(from_to.to);
                    
                    int count = getStreetSegmentCurvePointCount(*it);
                    t_point coord_inter = location_calculator(pos);
                    t_point coord_inter_1 = location_calculator(pos_1);
                    if (count == 0) {
                            auto street_name = Street_map_level1.find(iter->first);
                             if(street_name != Street_map_level1.end()){
                                 segmentcount++;
                            if(segmentcount==31){
                                setcolor(BLACK);
		                settextattrs(8,atan(((coord_inter_1.y)-(coord_inter.y))/((coord_inter_1.x)-(coord_inter.x)))/DEG_TO_RAD); 
		                drawtext(((coord_inter_1.x)+(coord_inter.x))/2, ((coord_inter_1.y)+(coord_inter.y))/2,  street_name->first,0.005,0.005);
                                segmentcount=0;
                                }
                            }       
                           }
                    }
                }       
            }
        
/*draw street name of level 2*/
void draw_street_name2(){
    int segmentcount=0;
    for(auto iter = Streetsegment_map.begin();iter!=Streetsegment_map.end();iter++ ){
        for (auto it = (iter->second).begin();it != (iter->second).end();it++){
            StreetSegmentEnds from_to =  getStreetSegmentEnds(*it);
            LatLon pos = getIntersectionPosition(from_to.from);
            LatLon pos_1 = getIntersectionPosition(from_to.to);
                    
                    int count = getStreetSegmentCurvePointCount(*it);
                    t_point coord_inter = location_calculator(pos);
                    t_point coord_inter_1 = location_calculator(pos_1);
                    if (count == 0) {
                            auto street_name = Street_map_level2.find(iter->first);
                             if(street_name != Street_map_level2.end()){
                                 segmentcount++;
                            if(segmentcount==9){
                                setcolor(BLACK);
		                settextattrs(8,atan(((coord_inter_1.y)-(coord_inter.y))/((coord_inter_1.x)-(coord_inter.x)))/DEG_TO_RAD); 
		                drawtext(((coord_inter_1.x)+(coord_inter.x))/2, ((coord_inter_1.y)+(coord_inter.y))/2,  street_name->first,0.002,0.002);
                                segmentcount=0;
                                }
                            }       
                           }
                    }
                }       
            }
      
/*draw street name of level 3*/
void draw_street_name3(){
    int segmentcount=0;
    for(auto iter = Streetsegment_map.begin();iter!=Streetsegment_map.end();iter++ ){
        for (auto it = (iter->second).begin();it != (iter->second).end();it++){
            StreetSegmentEnds from_to =  getStreetSegmentEnds(*it);
            LatLon pos = getIntersectionPosition(from_to.from);
            LatLon pos_1 = getIntersectionPosition(from_to.to);
                    
                    int count = getStreetSegmentCurvePointCount(*it);
                    t_point coord_inter = location_calculator(pos);
                    t_point coord_inter_1 = location_calculator(pos_1);
                    if (count == 0) {
                            auto street_name = Street_map_level3.find(iter->first);
                             if(street_name != Street_map_level3.end()){
                                 segmentcount++;
                            if(segmentcount==6){
                                setcolor(BLACK);
		                settextattrs(8,atan(((coord_inter_1.y)-(coord_inter.y))/((coord_inter_1.x)-(coord_inter.x)))/DEG_TO_RAD); 
		                drawtext(((coord_inter_1.x)+(coord_inter.x))/2, ((coord_inter_1.y)+(coord_inter.y))/2,  street_name->first,0.001,0.001);
                                segmentcount=0;
                                }
                            }       
                           }
                    }
                }       
            }

/*draw street name of level 4*/
void draw_street_name4(){
    int segmentcount=0;
    for(auto iter = Streetsegment_map.begin();iter!=Streetsegment_map.end();iter++ ){
        for (auto it = (iter->second).begin();it != (iter->second).end();it++){
            StreetSegmentEnds from_to =  getStreetSegmentEnds(*it);
            LatLon pos = getIntersectionPosition(from_to.from);
            LatLon pos_1 = getIntersectionPosition(from_to.to);
                
            int count = getStreetSegmentCurvePointCount(*it);
            t_point coord_inter = location_calculator(pos);
            t_point coord_inter_1 = location_calculator(pos_1);
            if (count == 0) {
                auto street_name = Street_map_level4.find(iter->first);
                if(street_name != Street_map_level4.end()){
                    segmentcount++;
                if(segmentcount==4){
                    setcolor(BLACK);
                    settextattrs(8,atan(((coord_inter_1.y)-(coord_inter.y))/((coord_inter_1.x)-(coord_inter.x)))/DEG_TO_RAD); 
		    drawtext(((coord_inter_1.x)+(coord_inter.x))/2, ((coord_inter_1.y)+(coord_inter.y))/2,  street_name->first,0.0009,0.0009);
                    segmentcount=0;
                    }
                }       
            }
        }
    }
}

/*Finding start points and end points*/
void find_coordinate()
{
    double max_lat = 0;
    double min_lat = 90;
    double max_lon = 180;
    double min_lon = -180;
    LatLon temp;
    for(auto iter = Intersection_map.begin();iter != Intersection_map.end();iter++){
        temp = getIntersectionPosition(iter->second);
        if(max_lat < temp.lat) {
            max_lat = temp.lat;
           
        }
        if(min_lat > temp.lat) {
            min_lat = temp.lat;
       
        }
        if(max_lon > temp.lon) {
            max_lon = temp.lon;
          
        }

         if(min_lon<temp.lon) {
            min_lon = temp.lon;
         
        }
               
    }
    Ave_lat = (max_lat + min_lat)/2;
    start_lat = min_lat;
    start_lon = max_lon*cos(Ave_lat*DEG_TO_RAD);
    end_lat = max_lat;
    end_lon = min_lon*cos(Ave_lat*DEG_TO_RAD);
    
}

unsigned long long find_closest_intersection(LatLon my_position){
    double closest=0;
    double distance=0;
    unsigned long long intersectionID=0;
    closest = find_distance_between_two_points(my_position,
            getIntersectionPosition(0));
    // Find the distance of my position and the first element in intersection structure.
    for (unsigned long long i=1;i<getNumberOfIntersections();i++) {
        distance = find_distance_between_two_points(my_position,
                getIntersectionPosition(i));

        if (distance < closest) {
            //Check if the distance is less than previous distance.
            closest = distance;
            distance=0;
            intersectionID = i;
        }
    }
    LatLon location=getIntersectionPosition(intersectionID);
      t_point coordinate=location_calculator(location);
    setcolor(RED);
     fillarc (coordinate.x,coordinate.y,0.005,0,360);
     setcolor(WHITE);
     settextattrs(10,0); 
     drawtext(coordinate.x, coordinate.y,  "I",0.2,0.2);
    return intersectionID;
}

void draw_instruction(vector<unsigned> Path){
    if(Path.size()!=0){
    int sum=0;
    int n=0;
//    t_bound_box current_visable = get_visible_world();
//    end_lon=current_visable.top_right().x;
//    start_lon=current_visable.bottom_left().x;
//    start_lat=current_visable.bottom_left().y;
//    end_lat=current_visable.top_right().y;
//    double start_lon1=end_lon-abs(start_lon-end_lon)*0.330645;
//    double start_lat1=start_lat+abs(start_lat-end_lat)*0.509485;
//    t_bound_box a = t_bound_box(start_lon1,start_lat1,end_lon,start_lat);
//    setcolor(WHITE);
//    fillrect(a);
//    setcolor(BLACK);
//    settextattrs(10,0); 
    for(unsigned iter_path = 0; iter_path < Path.size()-1;iter_path++){
       int a=getStreetSegmentStreetID(Path[iter_path]);
       int b=getStreetSegmentStreetID(Path[iter_path+1]);
       string name=getStreetName(a);
       string namenext=getStreetName(b);
       if(name==namenext)
           sum+=find_street_segment_length(Path[iter_path]);
       else{
            string length="Go ";
            length+=to_string(sum);
            string m=" m and turn ";
            length+=m;
            if(getStreetSegmentEnds(Path[iter_path]).from==getStreetSegmentEnds(Path[iter_path+1]).from){
               if(abs(getIntersectionPosition(getStreetSegmentEnds(Path[iter_path]).from).lon)>abs(getIntersectionPosition(getStreetSegmentEnds(Path[iter_path+1]).to).lon)){
                   string a="right to ";
                   length+=a;
               }
               else
               {
                   string a="left to ";
                   length+=a;
               }
               
            }
                if(getStreetSegmentEnds(Path[iter_path]).from==getStreetSegmentEnds(Path[iter_path+1]).to){
                     if(abs(getIntersectionPosition(getStreetSegmentEnds(Path[iter_path]).from).lon)>abs(getIntersectionPosition(getStreetSegmentEnds(Path[iter_path+1]).from).lon)){
                         string a="right to ";
                         length+=a;
                     }
                     else{
                         string a="left to ";
                         length+=a;
                     }
                }
                    if(getStreetSegmentEnds(Path[iter_path]).to==getStreetSegmentEnds(Path[iter_path+1]).from){
                        if(abs(getIntersectionPosition(getStreetSegmentEnds(Path[iter_path]).to).lon)>abs(getIntersectionPosition(getStreetSegmentEnds(Path[iter_path+1]).to).lon)){
                            string a="right to ";
                            length+=a;
                        }
                        else
                        {
                         string a="left to ";
                         length+=a;
                        }
                    }
                        if(getStreetSegmentEnds(Path[iter_path]).to==getStreetSegmentEnds(Path[iter_path+1]).to){
                             if(abs(getIntersectionPosition(getStreetSegmentEnds(Path[iter_path]).to).lon)>abs(getIntersectionPosition(getStreetSegmentEnds(Path[iter_path+1]).from).lon)){
                                 string a="right to ";
                                 length+=a;
                             }
                             else
                             {
                                 string a="left to ";
                                 length+=a;
                             }
                        }
            length+=namenext;
            //drawtext((start_lon1+abs(end_lon-start_lon1)/2),start_lat+abs(start_lat-start_lat1)*0.9799778+0.01*n ,  length,1,1);
            n++;
            cout<<length<<endl;
            sum=0;
       }
    }
    }
}

void initial(){
flag=false;
flagkey=false;
flagchange=false;
flaginter=false;
flagfindpath=false;
flagfindpathPOI=false;
checkfirst=false;
IDfrom=0;
IDto=0;
ratio=0;
start_lat=0;
start_lon=0;
aver_lag=0;
Ave_lat=0;
end_lat=0;
end_lon=0;
POI.clear();
findpath.clear();
findpathPOI.clear();
choosemap.clear();
findinter.clear();
path.clear();
}

string convert_to_low(string name){
  locale loc;
  for (size_t i=0; i<name.length(); i++)
    name[i]=tolower(name[i],loc);
  return name;
}
