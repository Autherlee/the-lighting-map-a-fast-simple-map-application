/* 
 * File:   Intersection.h
 * Author: wumin3
 *
 * Created on January 19, 2015, 1:36 PM
 */

#ifndef INTERSECTION_H
#define	INTERSECTION_H
#include <string>
using namespace std;


class my_intersection {
private :
    unsigned ID;
    string NAME ;
public:
    my_intersection(unsigned id,string name);
    unsigned get_ID();
    string get_NAME(); 
    
};


#endif	/* INTERSECTION_H */

