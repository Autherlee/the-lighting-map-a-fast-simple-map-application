#include "m3.h"
#include<string>

using namespace std; 

//This function can draw the path we found by passing the segment id.
void draw_PATH_FUN(unsigned seg_id ){
    StreetSegmentEnds from_to =  getStreetSegmentEnds(seg_id);
    LatLon pos = getIntersectionPosition(from_to.from);
    LatLon pos_1 = getIntersectionPosition(from_to.to);
                    
    int count = getStreetSegmentCurvePointCount(seg_id);
    t_point coord_inter = location_calculator(pos);
    t_point coord_inter_1 = location_calculator(pos_1);
    setlinewidth (3);
    setcolor (BLUE);
    if (count > 0) {
        t_point curve_tpoint = location_calculator(getStreetSegmentCurvePoint(seg_id,0));
        drawline(coord_inter.x,coord_inter.y,curve_tpoint.x,curve_tpoint.y);
        for (int i = 0; i < count-1; i++) {
            LatLon curve = getStreetSegmentCurvePoint(seg_id,i);
            t_point temp = location_calculator(curve);
            LatLon curve_1 = getStreetSegmentCurvePoint(seg_id,i+1);
            t_point temp_1 = location_calculator(curve_1);
            drawline(temp.x,temp.y,temp_1.x,temp_1.y);
        }
        t_point temp = location_calculator(getStreetSegmentCurvePoint(seg_id,count-1));
        drawline(temp.x,temp.y,coord_inter_1.x,coord_inter_1.y);
    }
    else
        drawline(coord_inter.x,coord_inter.y,coord_inter_1.x,coord_inter_1.y);       
}

// This function is the core in this milestone. Finding path bewteen two intersections
// using A* algorithm. Using priority queue as structure.
std::vector<unsigned> find_path_between_intersections(unsigned intersect_id_start, unsigned intersect_id_end) {
    //cout<<"debug1"<<endl;
    priority_queue<wavefront,vector<wavefront>,Mycom>intersection_need;
    intersection_need.push(wavefront(intersect_id_start,intersect_id_start,
            intersect_id_start, intersect_id_end,1600000,0));
    
    vector<wavefront> intersection_all_1 =intersection_all;
    vector<unsigned> path;
     if(intersect_id_start>getNumberOfIntersections()|| intersect_id_end>getNumberOfIntersections()){
         return path;
    }
    intersection_all_1[intersect_id_start].state = 2;
    intersection_all_1[intersect_id_start].source_id = intersect_id_start;
    intersection_all_1[intersect_id_start].dest_id = intersect_id_end;
    intersection_all_1[intersect_id_start].previous = intersect_id_start;
    intersection_all_1[intersect_id_start].seg_id = 0;
    
    wavefront current_inter = intersection_need.top();
    
    while(intersection_need.size() != 0) {
        
        current_inter = intersection_need.top();
        
        intersection_need.pop();
        
        intersection_all_1[current_inter.self_id].state = 2;
        
        if (current_inter.self_id == intersect_id_end)
            break;
        for (unsigned i = 0; i < getIntersectionStreetSegmentCount(current_inter.self_id); i++) {
            
            wavefront temp_inter;
            
            unsigned  segment = getIntersectionStreetSegment(current_inter.self_id,i);
            StreetSegmentEnds next_intersection = getStreetSegmentEnds(segment);
            
            if(!getStreetSegmentOneWay(segment)) {//Two way segment

                if (next_intersection.to == current_inter.self_id) {
                    if (intersection_all_1[next_intersection.from].state != 2 ) {
                        //unsigned lol = current_inter.seg_id;
                        
                        if(current_inter.seg_id == 1600000 || segment == 1600000){
                            temp_inter = wavefront(next_intersection.from,current_inter.self_id,
                            intersect_id_start, intersect_id_end, segment, current_inter.travel_time_here);
                        
                        }
                        else if (getStreetSegmentStreetID(current_inter.seg_id) == getStreetSegmentStreetID(segment)) {
                            temp_inter = wavefront(next_intersection.from,current_inter.self_id,
                            intersect_id_start, intersect_id_end, segment, current_inter.travel_time_here);
                        }
                        
                        else {
                            temp_inter = wavefront(next_intersection.from,current_inter.self_id,
                            intersect_id_start, intersect_id_end, segment, (current_inter.travel_time_here)+0.2500);
                        }
                        intersection_need.push(temp_inter);
                        intersection_all_1[next_intersection.from].state = 1;
                        if (intersection_all_1[next_intersection.from].state == 1 && 
                                intersection_all_1[next_intersection.from].travel_time_here > 
                                temp_inter.travel_time_here) {
                            intersection_all_1[next_intersection.from].travel_time_here = temp_inter.travel_time_here;
                            intersection_all_1[next_intersection.from].previous = temp_inter.previous;
                            intersection_all_1[next_intersection.from].seg_id = temp_inter.seg_id;
                            
                        }
                    }
                    

                }
                else {
                   if (intersection_all_1[next_intersection.to].state != 2) {
                       if(current_inter.seg_id == 1600000 || segment == 1600000){
                           temp_inter = wavefront(next_intersection.to,current_inter.self_id,
                            intersect_id_start, intersect_id_end, segment, current_inter.travel_time_here);
                       }
                       else if (getStreetSegmentStreetID(current_inter.seg_id) == getStreetSegmentStreetID(segment)) {
                            temp_inter = wavefront(next_intersection.to,current_inter.self_id,
                            intersect_id_start, intersect_id_end, segment, current_inter.travel_time_here);
                        }
                       
                        else {
                            temp_inter = wavefront(next_intersection.to,current_inter.self_id,
                            intersect_id_start, intersect_id_end, segment, (current_inter.travel_time_here)+0.2500);
                        }
                        intersection_need.push(temp_inter);
                       intersection_all_1[next_intersection.to].state = 1;
                        if (intersection_all_1[next_intersection.to].state == 1 && 
                                intersection_all_1[next_intersection.to].travel_time_here > 
                                temp_inter.travel_time_here) {
                            intersection_all_1[next_intersection.to].travel_time_here = temp_inter.travel_time_here;
                            intersection_all_1[next_intersection.to].previous = temp_inter.previous;
                            intersection_all_1[next_intersection.to].seg_id = temp_inter.seg_id;
                        }
                   }

                  
                }
                
            }
            else {//One way segment
                if (next_intersection.from == current_inter.self_id) {
                   if (intersection_all_1[next_intersection.to].state != 2) {
                       if(current_inter.seg_id == 1600000 || segment == 1600000){
                           temp_inter = wavefront(next_intersection.to,current_inter.self_id,
                            intersect_id_start, intersect_id_end, segment, current_inter.travel_time_here);
                       }
                       else if (getStreetSegmentStreetID(current_inter.seg_id) == getStreetSegmentStreetID(segment)) {
                            temp_inter = wavefront(next_intersection.to,current_inter.self_id,
                            intersect_id_start, intersect_id_end, segment, current_inter.travel_time_here);
                        }
                       
                        else {
                            temp_inter = wavefront(next_intersection.to,current_inter.self_id,
                            intersect_id_start, intersect_id_end, segment, (current_inter.travel_time_here)+0.2500);
                        }
                        intersection_need.push(temp_inter);
                        intersection_all_1[next_intersection.to].state = 1;
                        if (intersection_all_1[next_intersection.to].state == 1 && 
                                intersection_all_1[next_intersection.to].travel_time_here > 
                                temp_inter.travel_time_here) {
                            intersection_all_1[next_intersection.to].travel_time_here = temp_inter.travel_time_here;
                            intersection_all_1[next_intersection.to].previous = temp_inter.previous;
                           intersection_all_1[next_intersection.to].seg_id = temp_inter.seg_id;
                        }
                   }

                }       
            }
        }
            
    }// End of While
    
    while (intersection_all_1[current_inter.self_id].self_id != intersect_id_start) {
        
        path.push_back(intersection_all_1[current_inter.self_id].seg_id);
        
        current_inter.self_id = intersection_all_1[current_inter.self_id].previous;
    
    }
    
    reverse(path.begin(),path.end());
    
    
//    cout<<compute_path_travel_time(path)<<endl;
//   for(auto iter_path = path.begin(); iter_path != path.end();iter_path++){
//     draw_PATH_FUN(*iter_path);   }
   return path;
}



// This function calculate the total travel time for a particular path.
// Add 0.25 minutes/15 seconds if the street name is not same.
double compute_path_travel_time(const std::vector<unsigned>& path) {
    double sum = 0;
    std::string name1;
    if (path.size() != 0)
        name1 = getStreetName(getStreetSegmentStreetID(path[0]));
    for (auto iter = path.begin();iter != path.end();iter++) {
        double temp = find_segment_travel_time(*iter);
        sum = sum + temp;
        std::string name2 = getStreetName(getStreetSegmentStreetID(*iter));
        if (name1 != name2) {
            sum = sum + 0.25;
            name1 = name2;
        }
    }
    return sum;
}

//This function find the path from a intersection to a point of interest. First find
//which two points of interest is close to the starting intersection. Then find the nearest
//intersection to a point of interest by calculating the smallest distance between point of 
//interest and intersection. Finally use find_path_between_intersections to find the path.
std::vector<unsigned> find_path_to_point_of_interest(unsigned intersect_id_start,
        std::string point_of_interest_name) {
    
    vector<unsigned> path;
    vector<unsigned> POIid;
    priority_queue<Top_three_POI,vector<Top_three_POI>,Compare> Distance_POI;
    unsigned count = getNumberOfPointsOfInterest();
    for (unsigned i = 0; i < count; i++) {
        string nameofPOI = getPointOfInterestName(i);
        if (point_of_interest_name == nameofPOI) {
            POIid.push_back(i);
        }
    }
    
    if (POIid.empty())
        return path;
    
    LatLon locationofIntersection = getIntersectionPosition(intersect_id_start);
    Distance_POI.push(Top_three_POI(10000000,0));
    
    for (vector<unsigned>::iterator iter = POIid.begin(); iter != POIid.end(); iter++) {
        LatLon locationofPOI = getPointOfInterestPosition(*iter);
        double tempdistance = find_distance_between_two_points(locationofPOI,locationofIntersection);
        if (tempdistance < 10000000) {
            Distance_POI.push(Top_three_POI(tempdistance,*iter));
        }
    }
    
    vector<LatLon> coordinate;
    int size = POIid.size();
    while ((Distance_POI.size()-1) != 0) {
        coordinate.push_back(getPointOfInterestPosition(Distance_POI.top().ID));
        Distance_POI.pop();
    }
    
    LatLon coordinate1;
    LatLon coordinate2;
    
    if (size == 0) {
        return path;
    }
    else if (size == 1) {
        coordinate1 = coordinate[0];
    }
    else {//if (size == 2) {
        coordinate1 = coordinate[0];
        coordinate2 = coordinate[1];
    }
 
    unsigned Intersection_points1 = 0;
    unsigned Intersection_points2 = 0;
  
    double closestdistance1 = 10000000;
    double closestdistance2 = 10000000;

    
    for (unsigned i = 0; i < getNumberOfIntersections(); i++) {
        LatLon locationofIntersection_1 = getIntersectionPosition(i);
        if (size == 1) {
            double distance_temp1 = find_distance_between_two_points(coordinate1,
                    locationofIntersection_1);
            if (distance_temp1 < closestdistance1) {
                closestdistance1 = distance_temp1;
                Intersection_points1 = i;
            }
        }
        else {//if (size == 2) {
            double distance_temp1 = find_distance_between_two_points(coordinate1,
                    locationofIntersection_1);
            double distance_temp2 = find_distance_between_two_points(coordinate2,
                    locationofIntersection_1);
            if (distance_temp1 < closestdistance1) {
                closestdistance1 = distance_temp1;
                Intersection_points1 = i;
            }
            if (distance_temp2 < closestdistance2) {
                closestdistance2 = distance_temp2;
                Intersection_points2 = i;
            }
        }
    }
    
    if (size == 1) {
        double time1 = compute_path_travel_time(
        find_path_between_intersections(intersect_id_start,Intersection_points1));
        if (time1 == 0) {
            return path;
        }
        else {
            path = find_path_between_intersections(intersect_id_start,Intersection_points1);
        }
    }
    else {// if (size == 2) {
        double time1 = compute_path_travel_time(
        find_path_between_intersections(intersect_id_start,Intersection_points1));
        double time2 = compute_path_travel_time(
        find_path_between_intersections(intersect_id_start,Intersection_points2));
        if (time1 < time2) {
            if (time1 == 0) {
                path = find_path_between_intersections(intersect_id_start,Intersection_points2);
            }
            else {
                path = find_path_between_intersections(intersect_id_start,Intersection_points1);
            }
        }
        else {
            if (time2 == 0) {
                path = find_path_between_intersections(intersect_id_start,Intersection_points1);
            }
            else {
                path = find_path_between_intersections(intersect_id_start,Intersection_points2);
            }
        }
    }
    return path;
}



//void pre_cal_travel_time(unsigned intersect_id_start, std::vector<unsigned>intersections_to_traverse){
//   
//    priority_queue<wavefront,vector<wavefront>,Mycom>intersection_need;
//    //vector<edge> precal_result;
//    unsigned intersect_id_end =  intersections_to_traverse[0];   
//    unsigned counter = 0;
//    unsigned size_travel = intersections_to_traverse.size();
//    unordered_map<unsigned,edge> temp_cost_map;
//    
//    intersection_need.push(wavefront(intersect_id_start,intersect_id_start,
//            intersect_id_start, intersect_id_end,160000,0));
//    
//    vector<wavefront> intersection_all_1 =  intersection_all;
//    
//    
//    
//    intersection_all_1[intersect_id_start].state = 2;
//    intersection_all_1[intersect_id_start].source_id = intersect_id_start;
//    intersection_all_1[intersect_id_start].dest_id = intersect_id_end;
//    intersection_all_1[intersect_id_start].previous = intersect_id_start;
//    intersection_all_1[intersect_id_start].seg_id = 0;
//    
//    wavefront current_inter = intersection_need.top();
//    
//    while(intersection_need.size() != 0) {
//        
//        current_inter = intersection_need.top();
//        
//        intersection_need.pop();
//        
//        
//        intersection_all_1[current_inter.self_id].state = 2;
//       
//        
//        
//        auto iter_inter = find(intersections_to_traverse.begin(),intersections_to_traverse.end(),current_inter.self_id);
//        
//        
//        
//        if (iter_inter != intersections_to_traverse.end()){
//            
//            intersect_id_end = *iter_inter;
//            intersections_to_traverse.erase(iter_inter);
//            
//            
//            vector<unsigned> path;
//            unsigned id_fuk_up = current_inter.self_id;
//            //wavefront current_inter_copy = current_inter;
//           if(intersection_all_1[current_inter.self_id].self_id != intersect_id_start)
//            counter ++;
//             
//            while (intersection_all_1[current_inter.self_id].self_id != intersect_id_start) {
//                path.push_back(intersection_all_1[current_inter.self_id].seg_id);
//                current_inter.self_id = intersection_all_1[current_inter.self_id].previous;
//               // cout<< current_inter.self_id<<endl;
//                    
//            }
//            current_inter.self_id = id_fuk_up;
//            reverse(path.begin(),path.end());
//            
//           
//            edge temp_edge = edge(compute_path_travel_time(path),path);
//            
////            if(intersect_id_start ==99109 ){
////                  cout<<intersect_id_end<<endl;
////                  cout<<counter<<endl;
////                }
//            //double cost_lol = compute_path_travel_time(path);
//            //cout<<cost_lol<<"ssss"<<endl;
////            if(cost_lol==0){
////                cout<<"lolggggggg"<<endl;
////                for(unsigned m =0 ; m<path.size();m++){
////                    cout<<path[m]<<"lol"<<endl;
////                }
//         //   }
//            
//            temp_cost_map.insert(make_pair(intersect_id_end,temp_edge));
//            //cout<<temp_cost_map[intersect_id_end].cost<<endl;
//           // all_intersection_cost[intersect_id_start][intersect_id_end] = compute_path_travel_time(path);
//            
//           
//            
//            if(counter <size_travel-1){
//            
//               intersect_id_end =  intersections_to_traverse[counter]; 
//              
//            }
//            
//            else{
//                
//                
//             
//                break;
//            
//            }
//            
//        }
//        for (unsigned i = 0; i < getIntersectionStreetSegmentCount(current_inter.self_id); i++) {
//            
//            wavefront temp_inter;
//            
//            unsigned  segment = getIntersectionStreetSegment(current_inter.self_id,i);
//            StreetSegmentEnds next_intersection = getStreetSegmentEnds(segment);
//            
//            if(!getStreetSegmentOneWay(segment)) {//Two way segment
//
//                if (next_intersection.to == current_inter.self_id) {
//                    if (intersection_all_1[next_intersection.from].state != 2 ) {
//                       
//                        
//                        if(current_inter.seg_id == 160000 || segment == 160000){
//                            temp_inter = wavefront(next_intersection.from,current_inter.self_id,
//                            intersect_id_start, intersect_id_end, segment, current_inter.travel_time_here);
//                        
//                        }
//                        else if (getStreetSegmentStreetID(current_inter.seg_id) == getStreetSegmentStreetID(segment)) {
//                            temp_inter = wavefront(next_intersection.from,current_inter.self_id,
//                            intersect_id_start, intersect_id_end, segment, current_inter.travel_time_here);
//                        }
//                        
//                        else {
//                            temp_inter = wavefront(next_intersection.from,current_inter.self_id,
//                            intersect_id_start, intersect_id_end, segment, (current_inter.travel_time_here)+0.2500);
//                        }
//                        intersection_need.push(temp_inter);
//                        intersection_all_1[next_intersection.from].state = 1;
//                        if (intersection_all_1[next_intersection.from].state == 1 && 
//                                intersection_all_1[next_intersection.from].travel_time_here > 
//                                temp_inter.travel_time_here) {
//                            intersection_all_1[next_intersection.from].travel_time_here = temp_inter.travel_time_here;
//                            intersection_all_1[next_intersection.from].previous = temp_inter.previous;
//                            intersection_all_1[next_intersection.from].seg_id = temp_inter.seg_id;
//                            
//                        }
//                    }
//                    
//
//                }
//                else {
//                   if (intersection_all_1[next_intersection.to].state != 2) {
//                       if(current_inter.seg_id == 160000 || segment == 160000){
//                           temp_inter = wavefront(next_intersection.to,current_inter.self_id,
//                            intersect_id_start, intersect_id_end, segment, current_inter.travel_time_here);
//                       }
//                       else if (getStreetSegmentStreetID(current_inter.seg_id) == getStreetSegmentStreetID(segment)) {
//                            temp_inter = wavefront(next_intersection.to,current_inter.self_id,
//                            intersect_id_start, intersect_id_end, segment, current_inter.travel_time_here);
//                        }
//                       
//                        else {
//                            temp_inter = wavefront(next_intersection.to,current_inter.self_id,
//                            intersect_id_start, intersect_id_end, segment, (current_inter.travel_time_here)+0.2500);
//                        }
//                        intersection_need.push(temp_inter);
//                       intersection_all_1[next_intersection.to].state = 1;
//                        if (intersection_all_1[next_intersection.to].state == 1 && 
//                                intersection_all_1[next_intersection.to].travel_time_here > 
//                                temp_inter.travel_time_here) {
//                            intersection_all_1[next_intersection.to].travel_time_here = temp_inter.travel_time_here;
//                            intersection_all_1[next_intersection.to].previous = temp_inter.previous;
//                            intersection_all_1[next_intersection.to].seg_id = temp_inter.seg_id;
//                        }
//                   }
//
//                }
//                
//            }
//            else {//One way segment
//                if (next_intersection.from == current_inter.self_id) {
//                   if (intersection_all_1[next_intersection.to].state != 2) {
//                       if(current_inter.seg_id == 160000 || segment == 160000){
//                           temp_inter = wavefront(next_intersection.to,current_inter.self_id,
//                            intersect_id_start, intersect_id_end, segment, current_inter.travel_time_here);
//                       }
//                       else if (getStreetSegmentStreetID(current_inter.seg_id) == getStreetSegmentStreetID(segment)) {
//                            temp_inter = wavefront(next_intersection.to,current_inter.self_id,
//                            intersect_id_start, intersect_id_end, segment, current_inter.travel_time_here);
//                        }
//                       
//                        else {
//                            temp_inter = wavefront(next_intersection.to,current_inter.self_id,
//                            intersect_id_start, intersect_id_end, segment, (current_inter.travel_time_here)+0.2500);
//                        }
//                        intersection_need.push(temp_inter);
//                        intersection_all_1[next_intersection.to].state = 1;
//                        if (intersection_all_1[next_intersection.to].state == 1 && 
//                                intersection_all_1[next_intersection.to].travel_time_here > 
//                                temp_inter.travel_time_here) {
//                            intersection_all_1[next_intersection.to].travel_time_here = temp_inter.travel_time_here;
//                            intersection_all_1[next_intersection.to].previous = temp_inter.previous;
//                           intersection_all_1[next_intersection.to].seg_id = temp_inter.seg_id;
//                        }
//                   }
//
//                }       
//            }
//        }
//   
//        
//        
//    }// End of While
//    
//    all_intersection_cost.insert(make_pair(intersect_id_start,temp_cost_map));
//  
//    
//    
//    //cout<<all_intersection_cost[intersect_id_start][intersect_id_end].cost<<endl;
//    
//    return ;
//    
//    
//    
//    
//    
//    
//}



void pre_cal_travel_time(unsigned intersect_id_start, std::vector<unsigned>intersections_to_traverse){
    
    priority_queue<wavefront,vector<wavefront>,Mycom>intersection_need;
    
    unsigned intersect_id_end  = intersections_to_traverse[0] ;
    
    intersection_need.push(wavefront(intersect_id_start,intersect_id_start,
            intersect_id_start, intersect_id_end,1600000,0)); 
    
    vector<wavefront> intersection_all_1 =intersection_all;
    
    
    unordered_map<unsigned, edge> temp_cost_map;
    
    
    intersection_all_1[intersect_id_start].state = 2;
    
    intersection_all_1[intersect_id_start].source_id = intersect_id_start;
    
    intersection_all_1[intersect_id_start].dest_id = intersect_id_end;
    
    intersection_all_1[intersect_id_start].previous = intersect_id_start;
    
    intersection_all_1[intersect_id_start].seg_id = 0;
    
    wavefront current_inter = intersection_need.top();
    
    intersection_all_1[current_inter.self_id].state = 2;
        
    unsigned find_counter = 0;
    
    while(intersection_need.size() != 0) {
        //
        
        vector<unsigned> path;
        
        current_inter = intersection_need.top();
        
        intersection_need.pop();
        
        intersection_all_1[current_inter.self_id].state = 2;
//         if(find_counter >45){
//                    cout<<"lol"<<endl;                }
                
        
        auto inter_find  = find(intersections_to_traverse.begin(),intersections_to_traverse.end(),current_inter.self_id);
        
        if(inter_find != intersections_to_traverse.end()){
            
            auto iter_find_cost = temp_cost_map.find(current_inter.self_id) ; 
            
            if(iter_find_cost == temp_cost_map.end()){
            
                find_counter++;
//                if(find_counter >45){
//                    cout<<"lol"<<endl;                }
                
                
            
            unsigned temp_id = current_inter.self_id;
            
             while (intersection_all_1[current_inter.self_id].self_id != intersect_id_start) {
                 
                 path.push_back(intersection_all_1[current_inter.self_id].seg_id);
       
                 current_inter.self_id = intersection_all_1[current_inter.self_id].previous;
    
             }
            
            current_inter.self_id   =  temp_id ;
     
            reverse(path.begin(),path.end());
            
            
            
            
            edge temp_edge;
            
            temp_edge.cost = compute_path_travel_time(path);
            
            temp_edge.path = path;
            
            temp_cost_map.insert(make_pair(current_inter.self_id,temp_edge)) ;
            
           // cout<<temp_edge.cost << "    "<< current_inter.self_id<<"counter"<<find_counter<<endl;
            
            }
            
            if(find_counter == intersections_to_traverse.size()) 
            break;
        
        
        }
        for (unsigned i = 0; i < getIntersectionStreetSegmentCount(current_inter.self_id); i++) {
            
            wavefront temp_inter;
            
            unsigned  segment = getIntersectionStreetSegment(current_inter.self_id,i);
            StreetSegmentEnds next_intersection = getStreetSegmentEnds(segment);
            
            if(!getStreetSegmentOneWay(segment)) {//Two way segment

                if (next_intersection.to == current_inter.self_id) {
                    if (intersection_all_1[next_intersection.from].state != 2 ) {
                        //unsigned lol = current_inter.seg_id;
                        
                        if(current_inter.seg_id == 1600000 || segment == 1600000){
                            temp_inter = wavefront(next_intersection.from,current_inter.self_id,
                            intersect_id_start, intersect_id_end, segment, current_inter.travel_time_here);
                        
                        }
                        else if (getStreetSegmentStreetID(current_inter.seg_id) == getStreetSegmentStreetID(segment)) {
                            temp_inter = wavefront(next_intersection.from,current_inter.self_id,
                            intersect_id_start, intersect_id_end, segment, current_inter.travel_time_here);
                        }
                        
                        else {
                            temp_inter = wavefront(next_intersection.from,current_inter.self_id,
                            intersect_id_start, intersect_id_end, segment, (current_inter.travel_time_here)+0.2500);
                        }
                        intersection_need.push(temp_inter);
                        intersection_all_1[next_intersection.from].state = 1;
                        if (intersection_all_1[next_intersection.from].state == 1 && 
                                intersection_all_1[next_intersection.from].travel_time_here > 
                                temp_inter.travel_time_here) {
                            intersection_all_1[next_intersection.from].travel_time_here = temp_inter.travel_time_here;
                            intersection_all_1[next_intersection.from].previous = temp_inter.previous;
                            intersection_all_1[next_intersection.from].seg_id = temp_inter.seg_id;
                            
                        }
                    }
                    

                }
                else {
                   if (intersection_all_1[next_intersection.to].state != 2) {
                       if(current_inter.seg_id == 1600000 || segment == 1600000){
                           temp_inter = wavefront(next_intersection.to,current_inter.self_id,
                            intersect_id_start, intersect_id_end, segment, current_inter.travel_time_here);
                       }
                       else if (getStreetSegmentStreetID(current_inter.seg_id) == getStreetSegmentStreetID(segment)) {
                            temp_inter = wavefront(next_intersection.to,current_inter.self_id,
                            intersect_id_start, intersect_id_end, segment, current_inter.travel_time_here);
                        }
                       
                        else {
                            temp_inter = wavefront(next_intersection.to,current_inter.self_id,
                            intersect_id_start, intersect_id_end, segment, (current_inter.travel_time_here)+0.2500);
                        }
                        intersection_need.push(temp_inter);
                       intersection_all_1[next_intersection.to].state = 1;
                        if (intersection_all_1[next_intersection.to].state == 1 && 
                                intersection_all_1[next_intersection.to].travel_time_here > 
                                temp_inter.travel_time_here) {
                            intersection_all_1[next_intersection.to].travel_time_here = temp_inter.travel_time_here;
                            intersection_all_1[next_intersection.to].previous = temp_inter.previous;
                            intersection_all_1[next_intersection.to].seg_id = temp_inter.seg_id;
                        }
                   }

                  
                }
                
            }
            else {//One way segment
                if (next_intersection.from == current_inter.self_id) {
                   if (intersection_all_1[next_intersection.to].state != 2) {
                       if(current_inter.seg_id == 1600000 || segment == 1600000){
                           temp_inter = wavefront(next_intersection.to,current_inter.self_id,
                            intersect_id_start, intersect_id_end, segment, current_inter.travel_time_here);
                       }
                       else if (getStreetSegmentStreetID(current_inter.seg_id) == getStreetSegmentStreetID(segment)) {
                            temp_inter = wavefront(next_intersection.to,current_inter.self_id,
                            intersect_id_start, intersect_id_end, segment, current_inter.travel_time_here);
                        }
                       
                        else {
                            temp_inter = wavefront(next_intersection.to,current_inter.self_id,
                            intersect_id_start, intersect_id_end, segment, (current_inter.travel_time_here)+0.2500);
                        }
                        intersection_need.push(temp_inter);
                        intersection_all_1[next_intersection.to].state = 1;
                        if (intersection_all_1[next_intersection.to].state == 1 && 
                                intersection_all_1[next_intersection.to].travel_time_here > 
                                temp_inter.travel_time_here) {
                            intersection_all_1[next_intersection.to].travel_time_here = temp_inter.travel_time_here;
                            intersection_all_1[next_intersection.to].previous = temp_inter.previous;
                           intersection_all_1[next_intersection.to].seg_id = temp_inter.seg_id;
                        }
                   }

                }       
            }
        }
            
    }// End of While
    
     all_intersection_cost.insert(make_pair(intersect_id_start,temp_cost_map));
      
     
    
   

    return ;
    
    
    
    
    
    
    
    
    
    
  }