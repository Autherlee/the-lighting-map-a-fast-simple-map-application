/* 
 * File:   Street.h
 * Author: wumin3
 *
 * Created on January 19, 2015, 1:46 PM
 */

#ifndef STREET_H
#define	STREET_H
#include <string>
using namespace std;


class my_Street {
private:
    unsigned ID;
    string NAME;
    
public:
    my_Street(unsigned id,string name);
    unsigned get_ID ();
    string get_NAME ();
    
};

#endif	/* STREET_H */

