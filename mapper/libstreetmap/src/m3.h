/* 
 * File:   m3.h
 * Author: liyu19
 *
 * Created on March 9, 2015, 10:31 PM
 */

#ifndef M3_H
#define	M3_H

#include <vector>
#include <string>
#include "m1.h"
#include "m2.h"
#include<queue>
#include <algorithm>
#include "StreetsDatabaseAPI.h"
#include "wavefront.h"
using namespace std; 

std::vector<unsigned> find_path_between_intersections(unsigned intersect_id_start,unsigned intersect_id_end);

double compute_path_travel_time(const std::vector<unsigned>&path) ;


std::vector<unsigned> find_path_to_point_of_interest(unsigned intersect_id_start,std::string point_of_interest_name );

void draw_PATH_FUN(unsigned seg_id) ;

void pre_cal_travel_time(unsigned begin, std::vector<unsigned>intersections_to_traverse);

#endif	/* M3_H */

