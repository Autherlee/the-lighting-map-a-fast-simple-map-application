#pragma once
#include <string>
#include "graphics.h"
#include <vector>
#include <unordered_map>
#include "m1.h"
#include "StreetsDatabaseAPI.h"
// Draws the map whose at map_path; this should be a .bin file.
void draw_map(std::string map_path);

void drawscreen (void);
void draw_street(int draw_street_flag);
t_point location_calculator(LatLon coordinates);
void draw_highway();
void draw_feature(t_point* points, int npoints, string name);
void act_on_button_press(float x, float y, t_event_buttonPressed event);
void act_on_key_press (char c) ;
void act_on_new_button_func(void (*drawscreen_ptr) (void));
void act_on_new_button_func_key(void (*drawscreen_ptr) (void));
void act_on_new_button_func_change(void (*drawscreen_ptr) (void));
void act_on_new_button_func_intersection(void (*drawscreen_ptr) (void));
void act_on_new_button_func_path(void (*drawscreen_ptr) (void));
void act_on_new_button_func_poi(void (*drawscreen_ptr) (void));
void draw_point_of_interest();
void draw_search_POI();
void draw_choose_map();
void draw_intersection_map();
void draw_path_map(vector<unsigned> findpath);
bool check(string name);
bool checkPOI(string name);
void draw_street_name1();
void draw_street_name2();
void draw_street_name3();
void draw_street_name4();
void find_coordinate();
unsigned long long find_closest_intersection(LatLon my_position); 
void initial();
string convert_to_low(string name);
void draw_instruction(vector<unsigned> Path);