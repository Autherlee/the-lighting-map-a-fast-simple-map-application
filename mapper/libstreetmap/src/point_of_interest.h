/* 
 * File:   point_of_interest.h
 * Author: yujian4
 *
 * Created on January 20, 2015, 6:03 PM
 */

#ifndef POINT_OF_INTEREST_H
#define	POINT_OF_INTEREST_H
#include <string>
using namespace std;


class pointofinterest {
private:
    unsigned ID;
    string NAME;
    
public:
    pointofinterest(unsigned id,string name);
    unsigned get_ID ();
    string get_NAME ();
    
};


#endif	/* POINT_OF_INTEREST_H */

