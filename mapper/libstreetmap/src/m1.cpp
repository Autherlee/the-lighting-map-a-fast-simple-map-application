#include "m1.h"
#include <unordered_map>
#include <iostream>
#include <algorithm>
#include <string>
#include <sstream>
#include "wavefront.h"





using namespace std;
//load the map
unordered_map<string, unsigned> Street_map;
//Unordered_map to store street using street name as key.
unordered_map<string, unsigned> Intersection_map;
//Unordered_map to store all intersection using intersection name as key.
unordered_map<string, vector<unsigned>> Streetsegment_map;


//Unordered_map to store all street segments belong to
//one street, using street name as key.
unordered_map<string, vector<unsigned>> Street_intersection_map;
//Unordered_map to store all street intersection belong to 
//one street, using street name as key.
unordered_map<unsigned, unsigned> point_of_interest_map;
//Unordered_map to store point of interset using id as key.

unordered_map<string, vector<unsigned>> Features;
//Unordered_map to store feature using name as key.

unordered_map<string, unsigned> Street_map_level1;
//Unordered_map to store street level1 using name as key.
unordered_map<string, unsigned> Street_map_level2;
//Unordered_map to store street level2 using name as key.
unordered_map<string, unsigned> Street_map_level3;
//Unordered_map to store street level3 using name as key.
unordered_map<string, unsigned> Street_map_level4;
//Unordered_map to store street level4 using name as key.

vector<wavefront> intersection_all;
unordered_map<unsigned,unordered_map<unsigned,edge>> all_intersection_cost;
unsigned flag_out = 0;

bool load_map(std::string map_name) {

    bool load_success = loadStreetDatabaseBIN(map_name);
    for (unsigned i = 0; i < getFeatureCount(); i++) {
        vector<unsigned> feture_id;
        string Feature_type = getFeatureAttribute(i, "water");
        if (!Feature_type.empty()) {
             Features.insert(make_pair(Feature_type, feture_id));
        }
    }
    for (unsigned i = 0; i < getFeatureCount(); i++) {
        vector<unsigned> feture_id;
        string Feature_type = getFeatureAttribute(i, "waterway");
        if (!Feature_type.empty()) {
             Features.insert(make_pair(Feature_type, feture_id));
        }
    }
    for (unsigned i = 0; i < getFeatureCount(); i++) {
        vector<unsigned> feture_id;
        string Feature_type = getFeatureAttribute(i, "land");
        if (!Feature_type.empty()) {
             Features.insert(make_pair(Feature_type, feture_id));
        }
    }
    for (unsigned i = 0; i < getFeatureCount(); i++) {
        vector<unsigned> feture_id;
        string Feature_type = getFeatureAttribute(i, "natural");
        
                
                    if (Feature_type.empty()) {
                    Feature_type = getFeatureAttribute(i, "leisure");
                        if (Feature_type.empty()) {
                        Feature_type = getFeatureAttribute(i, "boundary");
                        }
                    }
                
        if (!Feature_type.empty())
            Features.insert(make_pair(Feature_type, feture_id));

    }

    for (unsigned i = 0; i < getFeatureCount(); i++) {
        string Feature_type = getFeatureAttribute(i, "natural");

        if (Feature_type.empty()) {
            Feature_type = getFeatureAttribute(i, "water");
            if (Feature_type.empty()) {
                Feature_type = getFeatureAttribute(i, "waterway");
                if (Feature_type.empty()) {
                    Feature_type = getFeatureAttribute(i, "land");
                    if (Feature_type.empty()) {
                    Feature_type = getFeatureAttribute(i, "leisure");
                    if (Feature_type.empty()) {
                    Feature_type = getFeatureAttribute(i, "boundary");
                    }
                }
            }
        }
    }       
        auto iter = Features.find(Feature_type);
        if (iter != Features.end()) {
            iter->second.push_back(i);
        }

    }





    for (unsigned i = 0; i < getNumberOfStreets(); i++) {
        vector<unsigned> myvec;
        vector<unsigned> myvec_intersection;
        Street_map.insert(make_pair(getStreetName(i), i));
        Streetsegment_map.insert(make_pair(getStreetName(i), myvec));
        Street_intersection_map.insert(make_pair(getStreetName(i), myvec_intersection));
        // Load street name to three unordered_map.
    } 
    //all_intersection_cost.resize(getNumberOfIntersections());
    //vector<unsigned> travel_time;
    for (unsigned i = 0; i < getNumberOfIntersections(); i++) {
        Intersection_map.insert(make_pair(getIntersectionName(i), i));
        wavefront temp = wavefront(i,0,0,0,0,1600000);
        intersection_all.push_back(temp);
        //travel_time.push_back(i);
        //travel_time.resize(getNumberOfIntersections());
       
       // all_intersection_cost[i].resize(getNumberOfIntersections());
        // Insert intersection to unordered_map.
        for (unsigned j = 0; j < getIntersectionStreetSegmentCount(i); j++) {
            unordered_map<string, vector<unsigned>>::iterator it_seg =
                    Street_intersection_map.find(getStreetName(getStreetSegmentStreetID(getIntersectionStreetSegment(i, j))));
            //Find which street the intersection belongs to.
            (it_seg->second).push_back(i);
           
            //Insert the intersection to Street_intersection_map.
        }
    }
   
    //vector<unsigned> t;
    
    for (unsigned i = 0; i < getNumberOfStreetSegments(); i++) {
        unordered_map<string, vector<unsigned>>::iterator it_seg =
                Streetsegment_map.find(getStreetName(getStreetSegmentStreetID(i)));
        //Find which street the street segment belongs to.
        (it_seg->second).push_back(i);
        //Insert the street segment to Streetsegment_map.
    } 

    for (unsigned i = 0; i < getNumberOfPointsOfInterest(); i++) {
        point_of_interest_map.insert(make_pair(i, i));
    }
    
    
    for (unordered_map<string, vector<unsigned>>::iterator it = Street_intersection_map.begin();
            it != Street_intersection_map.end(); it++) {
        sort(it->second.begin(), it->second.end());
        //Sort the vector of all intersection of one street.
        //In <algorithm>, there is a sort function that can sort vector in ascending order.
        
        vector<unsigned>::iterator it_inter = unique(it->second.begin(), it->second.end());
        
        //Make the vector unique, so that the vector will not store a intersection twice.
        //In <algorithm>, there is a unique function that can make vector value unique
        // the vector must be resize.
        
        it->second.resize(distance(it->second.begin(), it_inter));

    }
    for (auto iter = Street_map.begin(); iter != Street_map.end(); iter++) {
        double length = find_street_length(iter->first);
        double speedlimit = 0;
        vector<unsigned> street_segment = find_street_street_segments(iter->first);
        
        //Insert street segments into different levels.
        
        for (auto itercurve = street_segment.begin(); itercurve != street_segment.end(); itercurve++) {
            //um_curve_point+=getStreetSegmentCurvePointCount(*itercurve);
            speedlimit = getStreetSegmentSpeedLimit(*itercurve);
            
            //The first level of the map, these streets will draw at the beginning (Major streets)
            if (((length >=5000)&&(speedlimit >= 35))&&(iter->first != "(unknown)")) {

                Street_map_level1.insert(make_pair(iter->first, iter->second));
            }
            
            //Second level of the map, these streets will show up when users zooming in
            else if (((length < 5000&&length >=1000)&&(speedlimit >= 25))&&(iter->first != "(unknown)")) {

                Street_map_level2.insert(make_pair(iter->first, iter->second));
            }
            
            //Third level of the map, 
            else if (((length <1000&&length >=300)&&(speedlimit >= 15))&&(iter->first != "(unknown)")) {

                Street_map_level3.insert(make_pair(iter->first, iter->second));
            }
            else if(((length <300&&length >=0)&&(speedlimit >= 0))&&(iter->first != "(unknown)"))
                Street_map_level4.insert(make_pair(iter->first, iter->second));

        }
        //Street_map_level1.insert(make_pair("Queen's Park Crescent East", 2777));
        //Street_map_level1.insert(make_pair("Queen's Park Crescent West", 134));
        //Street_map_level1.insert(make_pair("Spadina Crescent", 253));
        //Street_map_level1.insert(make_pair("Queen's Park", 137)); 
    }

    return load_success;
}

std::vector<unsigned> find_intersection_street_segments(std::string intersection_name) {

    unordered_map<string, unsigned>::iterator it = Intersection_map.find(intersection_name);
    //Find the pointer pointing to this intersection.
    vector<unsigned> streetseg;
    for (unsigned i = 0; i < getIntersectionStreetSegmentCount(it->second); i++) {
        streetseg.push_back(getIntersectionStreetSegment(it->second, i));
        //Store all segments to a vector of unsigned integer.
    }
    return streetseg;
}

unsigned find_intersection_id_from_name(std::string intersection_name) {
    unordered_map<string, unsigned>::iterator it = Intersection_map.find(intersection_name);
    //Find the intersection by name.
    return it->second;
}

unsigned find_street_id_from_name(std::string street_name) {
    unordered_map<string, unsigned>::iterator it = Street_map.find(street_name);
    //Find the street by name.
    return it->second;
}

std::vector<std::string> find_intersection_street_names(std::string intersection_name) {
    vector<unsigned> street_seg = find_intersection_street_segments(intersection_name);
    //Get all the street segments by previous function.
    vector<string> street_names;
    //Create vector to store the street name.
    unsigned i = 0;
    for (vector<unsigned>::iterator it = street_seg.begin(); it != street_seg.end(); it++) {
        unsigned street_id = getStreetSegmentStreetID(street_seg[i]);
        vector<string>::iterator iter =
                find(street_names.begin(), street_names.end(), getStreetName(street_id));
        if (iter == street_names.end()) {//Check the street name is exist or not.
            street_names.push_back(getStreetName(street_id));
        }
        i++;
    }
    return street_names;
}

std::vector<std::string> find_intersection_street_names(unsigned intersection_id) {
    vector<unsigned> street_seg = find_intersection_street_segments(intersection_id);
    //Get all the street segments by previous function.
    vector<string> street_names;
    //Create vector to store the street name.
    unsigned i = 0;
    for (vector<unsigned>::iterator it = street_seg.begin(); it != street_seg.end(); it++) {
        unsigned street_id = getStreetSegmentStreetID(street_seg[i]);
        vector<string>::iterator iter =
                find(street_names.begin(), street_names.end(), getStreetName(street_id));
        if (iter == street_names.end()) {//Check the street name is exist or not.
            street_names.push_back(getStreetName(street_id));
        }
        i++;
    }
    return street_names;
}

std::vector<unsigned> find_street_street_segments(std::string street_name) {
    vector<unsigned> street_segments_id; //Create vector to store segments id.
    unordered_map<string, vector<unsigned>>::iterator it = Streetsegment_map.find(street_name);
    //Find all segments of one street by using the street name.
    street_segments_id = it->second;
    return street_segments_id;
}

std::vector<unsigned> find_intersection_street_segments(unsigned intersection_id) {
    vector<unsigned> vec; //Create vector to store all street segments of one intersection.
    unsigned count = getIntersectionStreetSegmentCount(intersection_id);
    for (unsigned i = 0; i < count; i++) {
        vec.push_back(getIntersectionStreetSegment(intersection_id, i));
    }
    return vec;
}

bool are_directly_connected(std::string intersection_name1, std::string intersection_name2) {
    if (intersection_name1 == intersection_name2) {
        //Check if two intersection are same. May increase the speed of this function.
        return true;
    }

    vector<unsigned> segments1 = find_intersection_street_segments(intersection_name1);
    vector<unsigned> segments2 = find_intersection_street_segments(intersection_name2);
    //Find all segments of these two intersection.
    for (vector<unsigned>::iterator itr1 = segments1.begin(); itr1 != segments1.end(); itr1++) {
        for (vector<unsigned>::iterator itr2 = segments2.begin(); itr2 != segments2.end(); itr2++) {
            if (*itr1 == *itr2) {//Check if two segments are same.
                if (getStreetSegmentOneWay(*itr1)) {
                    //Check one way, so that check "to" point to get result.
                    StreetSegmentEnds Ends = getStreetSegmentEnds(*itr1); //Get end points.
                    unsigned To = find_intersection_id_from_name(intersection_name2);
                    if (Ends.to == To) {//Check if directly connected.
                        return true; //If directly connected, terminate this function immediately.
                    }
                } else {
                    //If the segment is not one way, it means two intersections are directly connected.
                    return true;
                }
            }
        }
    }
    // Function can reach this line means these two intersection are not directly connected.
    return false;
}

std::vector<unsigned> find_adjacent_intersections(std::string intersection_name) {
    vector<unsigned> adjacent_intersection; //Create vector to store adjacent_intersections.
    vector<unsigned> streetseg = find_intersection_street_segments(intersection_name);
    //Find all street segments by using intersection name.
    unordered_map<string, unsigned>::iterator i = Intersection_map.find(intersection_name);
    //Find the intersection by name.
    for (vector<unsigned>::iterator it = streetseg.begin(); it != streetseg.end(); it++) {
        if (getStreetSegmentOneWay(*it)) {
            //Check the segment is one way. So that we can just find the "to" point.
            if (getStreetSegmentEnds(*it).to != i->second)
                adjacent_intersection.push_back(getStreetSegmentEnds(*it).to);
        } else {
            //If the segment is not one way. Find the id that is different from the original id.
            if (getStreetSegmentEnds(*it).to != i->second)
                adjacent_intersection.push_back(getStreetSegmentEnds(*it).to);
            else
                adjacent_intersection.push_back(getStreetSegmentEnds(*it).from);
        }
    }
    return adjacent_intersection;
}

double find_segment_travel_time(unsigned street_segment_id) {
    double distance = find_street_segment_length(street_segment_id) / 1000;
    //Get the distance of one street segment, in meter.
    double speed_limit = getStreetSegmentSpeedLimit(street_segment_id);
    //Get the speed limit.
    return distance / speed_limit * 60;
}

std::string find_closest_point_of_interest(LatLon my_position) {
    double closest;
    double distance;
    string point_of_interest;
    closest = find_distance_between_two_points(my_position,
            getPointOfInterestPosition(point_of_interest_map.begin()->first));
    // Find the distance of my position and the first element in point of interest structure.
    for (unordered_map<unsigned, unsigned>::iterator it = point_of_interest_map.begin();
            it != point_of_interest_map.end(); it++) {
        distance = find_distance_between_two_points(my_position,
                getPointOfInterestPosition(it->first));

        if (distance < closest) {
            //Check if the distance is less than previous distance.
            closest = distance;
            point_of_interest = getPointOfInterestName(it->second);
        }
    }
    return point_of_interest;
}

double find_distance_between_two_points(LatLon point1, LatLon point2) {
    double LatAve = (point1.lat + point2.lat) / 2;
    double x1 = (point1.lon * DEG_TO_RAD) * cos(LatAve * DEG_TO_RAD);
    double x2 = (point2.lon * DEG_TO_RAD) * cos(LatAve * DEG_TO_RAD);

    double dy = point2.lat * DEG_TO_RAD - point1.lat*DEG_TO_RAD;
    double dx = x2 - x1;

    return EARTH_RADIUS_IN_METERS * sqrt((dy)*(dy) + (dx * dx));
}

double find_street_segment_length(unsigned street_segment_id) {
    unsigned count = getStreetSegmentCurvePointCount(street_segment_id);
    //Get the count of curve points.
    StreetSegmentEnds Ends = getStreetSegmentEnds(street_segment_id);
    //Get the end points of a segment.
    LatLon point1 = getIntersectionPosition(Ends.from);
    LatLon point2 = getIntersectionPosition(Ends.to);

    if (count == 0) {
        //Check if count is zero. If zero, just calculate the two end points.
        return find_distance_between_two_points(point1, point2);
    } else {
        //The segment has curve points.
        double sum = find_distance_between_two_points(point1,
                getStreetSegmentCurvePoint(street_segment_id, 0));
        //Calculate the distance between first end point and first curve point.
        for (unsigned j = 0; j < count - 1; j++) {
            //Calculate the distance between all curve points.
            sum = sum + find_distance_between_two_points
                    (getStreetSegmentCurvePoint(street_segment_id, j)
                    , getStreetSegmentCurvePoint(street_segment_id, j + 1));
        }
        return sum + find_distance_between_two_points
                (getStreetSegmentCurvePoint(street_segment_id, count - 1), point2);
        //Add the distance between last curve point and second end point.
    }
}

std::vector<unsigned> find_all_street_intersections(std::string street_name) {
    vector<unsigned> itersection;
    unordered_map<string, vector<unsigned>>::iterator
    it_seg = Street_intersection_map.find(street_name);
    //Find the street in Street_intersection_map.
    itersection = it_seg->second;
    //Get the vector store in unordered_map.
    return itersection;
}

double find_street_length(std::string street_name) {
    vector<unsigned> seg_id = find_street_street_segments(street_name);
    //Get all street segments in unordered_map.
    double distance = 0;
    for (vector<unsigned>::iterator it = seg_id.begin(); it != seg_id.end(); it++) {
        distance = distance + find_street_segment_length(*it);
        //Add all street segments length together.
    }
    return distance;
}

//close the map

void close_map() {
    closeStreetDatabase();
    Street_map.clear();
    Intersection_map.clear();
    Streetsegment_map.clear();
    Street_intersection_map.clear();
    point_of_interest_map.clear();
    Features.clear();
    Street_map_level1.clear();
    Street_map_level2.clear();
    Street_map_level3.clear();
    Street_map_level4.clear();
}

