/* 
 * File:   wavelem.h
 * Author: liyu19
 *
 * Created on March 9, 2015, 11:26 PM
 */

#ifndef WAVEFRONT_H
#define	WAVEFRONT_H
#include "m1.h"
#include "StreetsDatabaseAPI.h"
#include<queue>
using namespace std;

double find_distance_between_two_points(LatLon point1, LatLon point2);
double find_segment_travel_time(unsigned street_segment_id);
struct wavefront{
    unsigned self_id;
    unsigned previous;
    unsigned source_id;
    unsigned dest_id;
    unsigned seg_id;
    unsigned state;
    double travel_time_here;
    double cost_to_dest;
    
    wavefront(){
        
    }
    
   // ~wavefront(){
        
   // }
    
    unsigned flag_out ;
    wavefront(unsigned id_1,unsigned from,unsigned id_3,unsigned id_4,unsigned id_5,double travel_time_previous){
        self_id = id_1;
        previous = from;
        source_id = id_3;
        dest_id = id_4;
        seg_id = id_5;
        state = 0;
        travel_time_here =0;
        cost_to_dest = 0;
        if(flag_out == 0){
       cost_to_dest = find_distance_between_two_points(getIntersectionPosition(self_id), getIntersectionPosition(dest_id));

      cost_to_dest = cost_to_dest/1651;
        }
        if(seg_id!=1600000) //time = find_segment_travel_time(seg_id);

       travel_time_here = travel_time_previous + find_segment_travel_time(seg_id);
        
        
    }
    
    bool operator<( wavefront& b)const {
        return ((travel_time_here + cost_to_dest) >(b.travel_time_here + b.cost_to_dest));

    }
    
   
    
};

struct Mycom{
     bool operator()(wavefront& b, wavefront&c)const{
        return ((c.travel_time_here + c.cost_to_dest) <(b.travel_time_here + b.cost_to_dest));

    }
    
    
};

struct Top_three_POI{
    double distance;
    unsigned ID;
    Top_three_POI(double distance_,unsigned ID_) {
	distance = distance_;
	ID = ID_;
    }
};

struct Compare {
    bool operator()(Top_three_POI& a, Top_three_POI& b)const {
        return (a.distance > b.distance);
    }
};




struct edge{
    //unsigned to;
    double cost;
    vector<unsigned> path;
    edge(){
        cost = 0;
        //path = (0)
    }
    edge(double travel_time,vector<unsigned> path_1){
        //to = to_where;
        cost = travel_time;
        path =path_1;    
    }
    
};


#endif	/* WAVELEM_H */



