/* 
 * File:   m4.h
 * Author: liyu19
 *
 * Created on March 21, 2015, 5:52 PM
 */

#ifndef M4_H
#define	M4_H
#include<vector>
#include "m1.h"
#include "m2.h"
#include "m3.h"

std::vector<unsigned> traveling_salesman111(std::vector<unsigned>intersections_to_traverse ,std::vector<unsigned>depot_locations);
std::vector<unsigned> traveling_salesman(std::vector<unsigned>intersections_to_traverse ,std::vector<unsigned>depot_locations);
double compute_cost(vector<unsigned> path);


#endif	/* M4_H */

