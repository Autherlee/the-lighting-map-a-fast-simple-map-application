#include "m4.h"
#include <unordered_map>
#include <time.h>
#include <stdlib.h>
#include <cmath>
#define TIME_LIMIT 30

std::vector<unsigned> traveling_salesman111(std::vector<unsigned>intersections_to_traverse ,std::vector<unsigned>depot_locations){
 //   double min_distance = find_distance_between_two_points(depot_locations[0],intersections_to_traverse[0]);
    clock_t start_time = clock();
    double temp = 10;
    double cool_rate =0.99;
    //unordered_map precal_info; 
//    unsigned size = intersections_to_traverse.size() + depot_locations.size() ;
      
    vector<unsigned> all_traverse_depot;
    
    all_traverse_depot.reserve(intersections_to_traverse.size() + depot_locations.size());
    
    all_traverse_depot.insert(all_traverse_depot.end(),intersections_to_traverse.begin(),intersections_to_traverse.end());
    
    all_traverse_depot.insert(all_traverse_depot.end(),depot_locations.begin(),depot_locations.end());
    
   //for(unsigned i =0; i<all_traverse_depot.size();i++){
  //      cout<<all_traverse_depot[i]<<endl;
   // }
    
//    for(unsigned i = 0; i<  all_traverse_depot.size();i++){
//        //cout<<all_traverse_depot.size()<<endl;
//       pre_cal_travel_time( all_traverse_depot[i], all_traverse_depot);
//        //cout<<all_intersection_cost[all_traverse_depot[i]][all_traverse_depot[i+1]].cost<<endl;
//       cout<<"lol1"<<endl;
//    }
   
    
    
    
    
    vector<unsigned> current_solution;
    vector<unsigned> new_solution;
  
    
    current_solution.push_back(depot_locations[0]);
    
    for(unsigned i = 0; i<  intersections_to_traverse.size();i++){
       
        //cout<<intersections_to_traverse[i]<<endl;
        current_solution.push_back(intersections_to_traverse[i]);
    
    }
    
    current_solution.push_back(depot_locations[0]);
     
//    for(unsigned i = 0; i<  current_solution.size();i++){
//        
//        //cout<<intersections_to_traverse[i]<<endl;
//        cout<<current_solution[i]<<endl;
//    
//    }
    
    //cout<<compute_cost(current_solution)<<endl;
 
    new_solution = current_solution;
    double current_cost = compute_cost(current_solution);
    double new_cost = current_cost;
    
    while(current_cost == 0){
     //  cout<<"lol3"<<endl;  
    unsigned size_swap = current_solution.size() -2;
   
    unsigned index_swap1 = rand() % size_swap + 1;
    
    unsigned inter_for_swap1 = current_solution[index_swap1];
    
    unsigned index_swap2 = rand() % size_swap + 1;
    
  
    //cout<<index_swap1<<" "<<index_swap2<<endl;
    
    unsigned inter_for_swap2 = current_solution[index_swap2];
    
    //cout<<index_swap1<<" "<<index_swap2<<endl;
   
    new_solution[index_swap1] = inter_for_swap2;
    
    
   
    new_solution[index_swap2] =  inter_for_swap1;
    
   
    
    current_solution = new_solution;
    
    
    
    current_cost = compute_cost(current_solution);
    
    }
    
    
    
    
    
    
    float timess =0 ;
    
    
    do{
    
      
     //  cout<<"lol4"<<endl; 
    clock_t currentTime = clock ();
    
    timess = ((float)(currentTime-start_time))/CLOCKS_PER_SEC ;    
   
    double current_cost = compute_cost(current_solution);
    
   
//    
     new_solution =  current_solution;
//    
//    
    unsigned size_swap = current_solution.size() -2;
//   
    unsigned index_swap1 = rand() % size_swap + 1;
//    

//    
  unsigned inter_for_swap1 = current_solution[index_swap1];
//    
  unsigned index_swap2 = rand() % size_swap + 1;
//    

//    
  unsigned inter_for_swap2 = current_solution[index_swap2];
//    
   new_solution[index_swap1] = inter_for_swap2;
  
  new_solution[index_swap2] =  inter_for_swap1;
//    
 new_cost = compute_cost(new_solution);
//    
while(new_cost == 0){
    //cout<<"lol4"<<endl;
    index_swap1 = rand() % size_swap + 1;
//    
//    if(index_swap1 == size_swap){
//        index_swap1 = index_swap1 -1;
//    }
//    
     inter_for_swap1 = current_solution[index_swap1];
//    
     index_swap2 = rand() % size_swap + 1;
//    
//    if(index_swap2 == size_swap){
//        index_swap2 = index_swap2 -1;
//    }
//    
//    if(index_swap1 == index_swap2)
//    {
//        index_swap1 = index_swap1 -1;
//    }
    inter_for_swap2 = current_solution[index_swap2];
//    
    new_solution[index_swap1] = inter_for_swap2;
   
   new_solution[index_swap2] =  inter_for_swap1;
    
   unsigned size_depot = depot_locations.size()-1;
   
   if(size_depot !=0){
    index_swap1 = rand() % size_depot;
     index_swap2 = rand() % size_depot; 
     new_solution[0] = depot_locations[index_swap1];
     new_solution[size_swap+1] = depot_locations[index_swap2];
    }    
    new_cost = compute_cost(new_solution);   
//        
    }
    

 
    if(new_cost < current_cost){
        current_cost = new_cost;
        
        //cout<<current_cost;
    }
    
    else{
        double if_accept = exp((current_cost - new_cost) / temp);
        
        double rand_rate = ((double)rand()) / (double)RAND_MAX; 
        
        if(if_accept > rand_rate){
            current_cost = new_cost ;
           // cout<<current_cost;
        }
        
        
    }
    
   
   
 
 
    temp = temp * cool_rate ; 
    
//    cout<<timess<<"lol"<<endl;
//    cout<<temp<<"lol11"<<endl;
    } while(timess< 27 && temp > 1);
    
//    for(unsigned i =0 ; i< current_solution.size();i++){
//        cout<<current_solution[i]<<endl;
//    }
//    
    
    vector<unsigned> final_path;
    for(unsigned i =0;i < current_solution.size()-1;i++){
        vector<unsigned> path_temp = all_intersection_cost[current_solution[i]][current_solution[i+1]].path ;
       for( unsigned j =0; j< path_temp.size();j++){
           final_path.push_back(path_temp[j]);
          }
        
    }
    
    
    return final_path;
    
}



double compute_cost(vector<unsigned> path){
    
    double cost =0;
    
    for(unsigned i =0;i<path.size()-1; i++){
       
       cost =  all_intersection_cost[path[i]][path[i+1]].cost +cost;
      //cout<<all_intersection_cost[path[i]][path[i+1]].cost<<endl;
       if(all_intersection_cost[path[i]][path[i+1]].cost == 0){
        //cout<<path[i]<<"     "<<path[i+1]<<endl;
           cost = 0;
           
           break;
       }
    }
   
    //cout<<"end"<<endl;
    return cost;
    
}

std::vector<unsigned> traveling_salesman(std::vector<unsigned>intersections_to_traverse ,std::vector<unsigned>depot_locations)

{
    
    vector<unsigned> all_traverse_depot;
    
    vector<unsigned> current_inter_path ;
    
//    clock_t start_time = clock();
//    double temp = 1000;
//    double cool_rate =0.001;
    
    all_traverse_depot.reserve(intersections_to_traverse.size() + depot_locations.size());
    
    all_traverse_depot.insert(all_traverse_depot.end(),intersections_to_traverse.begin(),intersections_to_traverse.end());
    
    all_traverse_depot.insert(all_traverse_depot.end(),depot_locations.begin(),depot_locations.end());
    
//  
    
    
    if(all_traverse_depot.size() < 49) {
        
        flag_out = 1;
    
//      for(unsigned i =0 ; i <all_traverse_depot.size(); i++) {
//       // cout<<"lol11"<<endl;
//        pre_cal_travel_time(all_traverse_depot[i],  all_traverse_depot) ;
//    
//    }
//        
//        
//        
//      vector<unsigned> current_solution;
//    vector<unsigned> new_solution;
//  
//    
//    current_solution.push_back(depot_locations[0]);
//    
//    for(unsigned i = 0; i<  intersections_to_traverse.size();i++){
//       
//        //cout<<intersections_to_traverse[i]<<endl;
//        current_solution.push_back(intersections_to_traverse[i]);
//    
//    }
//    
//    current_solution.push_back(depot_locations[0]);    
//        
//    new_solution = current_solution;
//    double current_cost = compute_cost(current_solution);
//    double new_cost = current_cost;
//    
//    while(current_cost == 0){
////     cout<<"lol3"<<endl;  
//    unsigned size_swap = current_solution.size() -2;
//   
//    unsigned index_swap1 = rand() % size_swap + 1;
//    
//    unsigned inter_for_swap1 = current_solution[index_swap1];
//    
//    unsigned index_swap2 = rand() % size_swap + 1;
//    
//  
//    //cout<<index_swap1<<" "<<index_swap2<<endl;
//    
//    unsigned inter_for_swap2 = current_solution[index_swap2];
//    
//    //cout<<index_swap1<<" "<<index_swap2<<endl;
//   
//    new_solution[index_swap1] = inter_for_swap2;
//    
//    
//   
//    new_solution[index_swap2] =  inter_for_swap1;
//    
//   
//    
//    current_solution = new_solution;
//    
//    
//    
//    current_cost = compute_cost(current_solution);
//    
//    }
//    
//    
//    
//    
//    
//    
//    float timess =0 ;
//    
//    
//    do{
//    
//      
////       cout<<"lol4"<<endl; 
//    clock_t currentTime = clock ();
//    
//    timess = ((float)(currentTime-start_time))/CLOCKS_PER_SEC ;    
//   
//    double current_cost = compute_cost(current_solution);
//    
//   
////    
//     new_solution =  current_solution;
////    
////    
//    unsigned size_swap = current_solution.size() -2;
////   
//    unsigned index_swap1 = rand() % size_swap + 1;
////    
//
////    
//  unsigned inter_for_swap1 = current_solution[index_swap1];
////    
//  unsigned index_swap2 = rand() % size_swap + 1;
////    
//
////    
//  unsigned inter_for_swap2 = current_solution[index_swap2];
////    
//   new_solution[index_swap1] = inter_for_swap2;
//  
//  new_solution[index_swap2] =  inter_for_swap1;
////    
// new_cost = compute_cost(new_solution);
////    
//while(new_cost == 0){
////    cout<<"lol4"<<endl;
//    index_swap1 = rand() % size_swap + 1;
////    
////    if(index_swap1 == size_swap){
////        index_swap1 = index_swap1 -1;
////    }
////    
//     inter_for_swap1 = current_solution[index_swap1];
////    
//     index_swap2 = rand() % size_swap + 1;
////    
////    if(index_swap2 == size_swap){
////        index_swap2 = index_swap2 -1;
////    }
////    
////    if(index_swap1 == index_swap2)
////    {
////        index_swap1 = index_swap1 -1;
////    }
//    inter_for_swap2 = current_solution[index_swap2];
////    
//    new_solution[index_swap1] = inter_for_swap2;
//   
//   new_solution[index_swap2] =  inter_for_swap1;
//    
//   unsigned size_depot = depot_locations.size()-1;
//   
//   if(size_depot !=0){
//    index_swap1 = rand() % size_depot;
//     index_swap2 = rand() % size_depot; 
//     new_solution[0] = depot_locations[index_swap1];
//     new_solution[size_swap+1] = depot_locations[index_swap2];
//    }    
//    new_cost = compute_cost(new_solution);   
////        
//    }
//    
//
// 
//    if(new_cost < current_cost){
//        current_cost = new_cost;
//        
//        //cout<<current_cost;
//    }
//    
//    else{
//        double if_accept = exp((current_cost - new_cost) / temp);
//        
//        double rand_rate = ((double)rand()) / (double)RAND_MAX; 
//        
//        if(if_accept > rand_rate){
//            current_cost = new_cost ;
//           // cout<<current_cost;
//        }
//        
//        
//    }
//    
//   
//   
// 
// 
//    temp = temp * (1-cool_rate) ; 
//    
////    cout<<timess<<"lol"<<endl;
////    cout<<temp<<"lol11"<<endl;
//    } while(timess< 27 && temp > 1);
//    
////    
////    for(unsigned i =0; i < current_solution.size();i++){
////        cout<<current_solution[i]<<endl ; 
////        
////        
////    }
//    
//     vector<unsigned> final_path;  
//     for (unsigned i = 0; i< current_solution.size()-1 ; i++){
//         
//        vector<unsigned> temp_path  = all_intersection_cost[current_solution[i]][current_solution[i+1]].path ; 
//        for (unsigned j = 0;j< temp_path.size();j++ ){
//            final_path.push_back(temp_path[j]) ;
//        }
//           
//     }
//    
//    
//    
//    return  final_path;
    
    for(unsigned i =0 ; i <all_traverse_depot.size(); i++) {
       // cout<<"lol11"<<endl;
        pre_cal_travel_time(all_traverse_depot[i],  all_traverse_depot) ;
    
    }
//    
    
     current_inter_path.push_back(depot_locations[0]) ;
    
     
     double min_distance = 100000; 
     
     unsigned index = 0;
     
     vector<unsigned> temp_intersections_to_traverse = intersections_to_traverse;
     
     for(unsigned i = 0; i <intersections_to_traverse.size() ; i++ ){
         
         for(unsigned j =0; j<intersections_to_traverse.size() ;j++){
             
             //double cost_temp = find_distance_between_two_points(getIntersectionPosition(current_inter_path[i]),getIntersectionPosition(intersections_to_traverse[j]));
             double cost_temp = all_intersection_cost[current_inter_path[i]][intersections_to_traverse[j]].cost ;
            //cout<<"cost1"<<endl;
                
             auto iter_inter_finder = find(current_inter_path.begin(),current_inter_path.end(),intersections_to_traverse[j]);
             
                 if(min_distance > cost_temp && cost_temp != 0 && iter_inter_finder == current_inter_path.end()){
                     
                     min_distance  = cost_temp; 
                    // cout<<"jinlejici"<<endl;
                     index = j;
                     
                 }
         }
         
        // cout<< min_distance<<endl;
         min_distance = 1000000;
         
         current_inter_path.push_back(intersections_to_traverse[index]);
        
     // cout<<intersections_to_traverse[index]<<endl;
         
     }
     
     index = 0;
     for(unsigned i = 0; i < depot_locations.size();i++){
        //cout<<"cost2"<<endl;
         //double cost_temp = find_distance_between_two_points(getIntersectionPosition(current_inter_path[intersections_to_traverse.size()]), getIntersectionPosition(depot_locations[i])); 
         
         double cost_temp = all_intersection_cost[current_inter_path[intersections_to_traverse.size()]][depot_locations[i]].cost;
         //cout<<current_inter_path[intersections_to_traverse.size()]<<"lol"<<endl;
         
        if(min_distance > cost_temp && cost_temp != 0){
            
            min_distance  = cost_temp;
            index = i;
         }
         
         
     }
      
     //cout<< min_distance<<endl;
     current_inter_path.push_back(depot_locations[index]);
     //cout<<depot_locations[index]<<endl;
    //cout<<all_intersection_cost[][]<<endl;  
    vector<unsigned> final_path;  
     for (unsigned i = 0; i< current_inter_path.size()-1 ; i++){
         
        vector<unsigned> temp_path  = all_intersection_cost[current_inter_path[i]][current_inter_path[i+1]].path ; 
        for (unsigned j = 0;j< temp_path.size();j++ ){
            final_path.push_back(temp_path[j]) ;
        }
           
         //cout<<"cost3"<<endl;
//       vector<unsigned> temp_path = find_path_between_intersections(current_inter_path[i], current_inter_path[i+1]);
//        for (unsigned j = 0;j< temp_path.size();j++ ){
//        final_path.push_back(temp_path[j]) ;
//       }
     
     
     
     }

       
    
    
    
    return final_path ;
    }
    
    
   else {
//        
//        
//        for(unsigned i =0 ; i <all_traverse_depot.size(); i++) {
//       // cout<<"lol11"<<endl;
//        pre_cal_travel_time(all_traverse_depot[i],  all_traverse_depot) ;
//    
//    }
//    
      //  vector<unsigned> ff_final_path;

       flag_out =0;
        
     current_inter_path.push_back(depot_locations[0]) ;
    
     
     double min_distance = 100000; 
     
     unsigned index = 0;
     
     vector<unsigned> temp_intersections_to_traverse = intersections_to_traverse;
     
     for(unsigned i = 0; i <intersections_to_traverse.size() ; i++ ){
         
         for(unsigned j =0; j<intersections_to_traverse.size() ;j++){
             
             double cost_temp = find_distance_between_two_points(getIntersectionPosition(current_inter_path[i]),getIntersectionPosition(intersections_to_traverse[j]));
            // double cost_temp = all_intersection_cost[current_inter_path[i]][intersections_to_traverse[j]].cost ;
            //cout<<"cost1"<<endl;
                
             auto iter_inter_finder = find(current_inter_path.begin(),current_inter_path.end(),intersections_to_traverse[j]);
             
                 if(min_distance > cost_temp && cost_temp != 0 && iter_inter_finder == current_inter_path.end()){
                     
                     min_distance  = cost_temp; 
                    // cout<<"jinlejici"<<endl;
                     index = j;
                     
                 }
         }
         
        // cout<< min_distance<<endl;
         min_distance = 1000000;
         
         current_inter_path.push_back(intersections_to_traverse[index]);
        
     // cout<<intersections_to_traverse[index]<<endl;
         
     }
     
     index = 0;
     for(unsigned i = 0; i < depot_locations.size();i++){
        //cout<<"cost2"<<endl;
         double cost_temp = find_distance_between_two_points(getIntersectionPosition(current_inter_path[intersections_to_traverse.size()]), getIntersectionPosition(depot_locations[i])); 
         
         //double cost_temp = all_intersection_cost[current_inter_path[intersections_to_traverse.size()]][depot_locations[i]].cost;
         //cout<<current_inter_path[intersections_to_traverse.size()]<<"lol"<<endl;
         
        if(min_distance > cost_temp && cost_temp != 0){
            
            min_distance  = cost_temp;
            index = i;
            
         }
         
         
     }
      
     //cout<< min_distance<<endl;
     current_inter_path.push_back(depot_locations[index]);
     //cout<<depot_locations[index]<<endl;
    //cout<<all_intersection_cost[][]<<endl;  
    vector<unsigned> final_path;  
     for (unsigned i = 0; i< current_inter_path.size()-1 ; i++){
         
//        vector<unsigned> temp_path  = all_intersection_cost[current_inter_path[i]][current_inter_path[i+1]].path ; 
//        for (unsigned j = 0;j< temp_path.size();j++ ){
//            final_path.push_back(temp_path[j]) ;
//        }
           
         //cout<<"cost3"<<endl;
       vector<unsigned> temp_path = find_path_between_intersections(current_inter_path[i], current_inter_path[i+1]);
        for (unsigned j = 0;j< temp_path.size();j++ ){
        final_path.push_back(temp_path[j]) ;
       }
     
     
     
     }

       
    
    
    
    return final_path ;
        
        
        
        
        
        
        
        
        
        
    }
    
    
    
  
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
//    
//    
}