/* 
 * File:   streetSegment.h
 * Author: wumin3
 *
 * Created on January 19, 2015, 1:53 PM
 */

#ifndef STREETSEGMENT_H
#define	STREETSEGMENT_H
#include <string>
using namespace std;


class my_Streetsegment {
private:
    unsigned ID;
    string NAME;
    
public:
    my_Streetsegment(unsigned id,string name);
    unsigned get_ID();
    string get_NAME();
    
    
};


#endif	/* STREETSEGMENT_H */

