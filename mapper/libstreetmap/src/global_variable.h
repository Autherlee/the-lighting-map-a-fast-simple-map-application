/* 
 * File:   global_variable.h
 * Author: liyu19
 *
 * Created on January 21, 2015, 2:42 PM
 */

#ifndef GLOBAL_VARIABLE_H
#define	GLOBAL_VARIABLE_H
#include <unordered_map>
#include <iostream>
#include <string>
#include "wavefront.h"

using namespace std;

extern  unordered_map<string, unsigned> Street_map;
extern  unordered_map<string, unsigned> Intersection_map;
extern  unordered_map<string,vector<unsigned>> Streetsegment_map;
extern  unordered_map<unsigned, unsigned> point_of_interest_map;
extern  unordered_map<string, vector<unsigned>> Street_intersection_map;
extern  unordered_map<string, vector<unsigned>> Features;
extern  unordered_map<unsigned,vector<LatLon>> Features_location;
extern  unordered_map<string, unsigned> Street_map_level1;
extern  unordered_map<string, unsigned> Street_map_level2;
extern  unordered_map<string, unsigned> Street_map_level3;
extern  unordered_map<string, unsigned> Street_map_level4;
extern  vector<wavefront> intersection_all;
extern  unordered_map<unsigned,unordered_map<unsigned,edge>> all_intersection_cost; 
extern  double ratio ;
extern  double start_lat;
extern  double start_lon;
extern  double aver_lag;
extern  double Ave_lat;
extern  double end_lat;
extern  double end_lon;
extern  unsigned flag_out; 

//extern  unordered_map<string, vector<unsigned> > Streetsegmentwithname_map;

#endif	/* GLOBAL_VARIABLE_H */

